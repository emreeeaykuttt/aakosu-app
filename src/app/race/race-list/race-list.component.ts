import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { registerElement } from 'nativescript-angular/element-registry';
import { SelectedIndexChangedEventData, ValueList } from 'nativescript-drop-down';
import { ObservableArray } from 'tns-core-modules/data/observable-array';
import { RadListViewComponent } from 'nativescript-ui-listview/angular';
import { ListViewEventData } from 'nativescript-ui-listview';
import * as moment from 'moment';

import { RaceService } from '../../services/race.service';
import { Race } from '../../models/race.model';

@Component({
    selector: 'RaceList',
    templateUrl: './race-list.component.html',
    styleUrls: ['./race-list.component.css']
})
export class RaceListComponent implements OnInit {
    
    loading = false;
    monthId = '0';
    cityId = '0';
    months: ValueList<string>;
    cities: ValueList<string>;
    _raceItems: ObservableArray<Race> = new ObservableArray([]); 
    @ViewChild('listView', { static: false }) listView: RadListViewComponent;

    constructor(private router: Router, private raceService: RaceService, private _changeDetectionRef: ChangeDetectorRef) {
        let trLocale = require('moment/locale/tr');
        moment.locale('tr', trLocale);
    }

    public get raceItems(): ObservableArray<Race> {
        return this._raceItems;
    }

    ngOnInit(): void {
        this.months = new ValueList<string>([
            {value: '0', display: 'Tüm Aylar'},
            {value: '1', display: 'Ocak'},
            {value: '2', display: 'Şubat'},
            {value: '3', display: 'Mart'},
            {value: '4', display: 'Nisan'},
            {value: '5', display: 'Mayıs'},
            {value: '6', display: 'Haziran'},
            {value: '7', display: 'Temmuz'},
            {value: '8', display: 'Ağustos'},
            {value: '9', display: 'Eylül'},
            {value: '10', display: 'Ekim'},
            {value: '11', display: 'Kasım'},
            {value: '12', display: 'Aralık'}
        ]);

        this.cities = new ValueList<string>([
            {value: '0', display: 'Tüm Şehirler'},
            {value: '34', display: 'İstanbul'},
            {value: '6', display: 'Ankara'},
            {value: '7', display: 'Antalya'},
            {value: '16', display: 'Bursa'},
            {value: '35', display: 'İzmir'},
        ]);
        
        this.loadRaceList();
    }

    loadRaceList(runLoading: boolean = true) {
        this.loading = runLoading;
        this.raceService.getRaces()
            .subscribe((result) => {
                this._raceItems.push(result);
                this.loading = false;
            }, (error) => {
                console.log(error);
            });
    }

    onPullToRefreshInitiated(args: ListViewEventData) {
        const that = new WeakRef(this);
        
        setTimeout(function () {
            that.get()._raceItems = new ObservableArray([])
            that.get().loadRaceList(false);
            const listView = args.object;
            listView.notifyPullToRefreshFinished();
        }, 1000);
    }

    monthChange(args: SelectedIndexChangedEventData) {
        this.monthId = this.months.getValue(args.newIndex);
        this.loading = true;

        this.raceService.getRacesFilter({
                monthId: this.monthId,
                cityId: this.cityId,
            })
            .subscribe((result) => {
                this._raceItems = new ObservableArray([])
                this._raceItems.push(result);
                this.loading = false;
            }, (error) => {
                console.log(error);
            });
    }

    cityChange(args: SelectedIndexChangedEventData) {
        this.cityId = this.cities.getValue(args.newIndex);
        this.loading = true;

        this.raceService.getRacesFilter({
                monthId: this.monthId,
                cityId: this.cityId,
            })
            .subscribe((result) => {
                this._raceItems = new ObservableArray([])
                this._raceItems.push(result);
                this.loading = false;
            }, (error) => {
                console.log(error);
            });
    }

    raceType(value: string, isUser: number) {
        if (value == 'single') {
            if (isUser == 1) {
                return '~/icons/runner-white.png';   
            } else {
                return '~/icons/runner-black.png';
            }
        }
        else if (value == 'team') {
            if (isUser == 1) {
                return '~/icons/runners-white.png';
            } else {
                return '~/icons/runners-black.png';
            }
        }
    }

    dateDayNumber(value: Date) {
        if (value) {
            return moment(String(value)).format('DD');
        }
    }

    dateDayText(value: Date) {                
        if (value) {
            return moment(String(value)).format('dddd')
        }
    }

    timeConvert(value: Date) {
        if (value) {
            return moment(String(value)).format('HH:mm')
        }
    }

}
