import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { Subscription } from 'rxjs';
import { ObservableArray } from 'tns-core-modules/data/observable-array';
import { RadListViewComponent } from 'nativescript-ui-listview/angular';
import { ListViewEventData } from 'nativescript-ui-listview';
import { EventData } from 'tns-core-modules/data/observable';
import { TextView } from 'tns-core-modules/ui/text-view';

import { UserService } from '~/app/services/user.service';
import { User } from '~/app/models/user.model';

@Component({
    selector: 'RaceRegisteredUsers',
    moduleId: module.id,
    templateUrl: './race-registered-users.component.html',
    styleUrls: ['./race-registered-users.component.css']
})
export class RaceRegisteredUsersComponent implements OnInit {

    loading = false;
    raceId: number;
    user: User;
    subscription: Subscription;
    _userItems: ObservableArray<User> = new ObservableArray([]); 
    @ViewChild('friendList', { static: false }) friendList: RadListViewComponent;

    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private routerExtension: RouterExtensions,
    ) { }

    public get userItems(): ObservableArray<User> {
        return this._userItems;
    }

    ngOnInit(): void {
        this.subscription = this.route.params.subscribe(params => {
            this.raceId = +params['id'];
        });

        this.loadUserList();
    }

    onNavBtnTap(){
        this.routerExtension.navigate(['race-detail/' + this.raceId], { relativeTo: this.route.parent });
    }

    loadUserList(runLoading: boolean = true, searchText: string = '') {
        this.loading = runLoading;
        this.userService.getRaceRegisteredUsers(this.raceId, searchText)
            .subscribe((result) => {
                this._userItems = new ObservableArray([]);
                if (result != '') {
                    this._userItems.push(result);
                } else {
                }
                this.loading = false;
            }, (error) => {
                console.log(error);
            });
    }

    onPullToRefreshInitiated(args: ListViewEventData) {
        const that = new WeakRef(this);
        
        setTimeout(function () {
            that.get()._userItems = new ObservableArray([])
            that.get().loadUserList(false);
            const listView = args.object;
            listView.notifyPullToRefreshFinished();
        }, 1000);
    }

    userSearch(args: EventData) {
        const searchText = args.object as TextView;
        this.loadUserList(true, searchText.text);
    }

}
