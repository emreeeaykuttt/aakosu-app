import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { Subscription } from 'rxjs';
import { Accuracy } from 'tns-core-modules/ui/enums';
import * as platform from 'tns-core-modules/platform';
import * as utils from 'tns-core-modules/utils/utils';
import * as moment from 'moment';
import * as dialogs from 'tns-core-modules/ui/dialogs';
import * as geolocation from 'nativescript-geolocation';
import * as http from 'tns-core-modules/http';

import { RaceService } from '../../services/race.service';
import { RaceUserService } from '../../services/race-user.service';
import { Race } from '../../models/race.model';
import { RaceUser } from '../../models/race-user.model';

var Calendar = require("nativescript-calendar");

@Component({
    selector: 'RaceDetail',
    moduleId: module.id,
    templateUrl: './race-detail.component.html',
    styleUrls: ['./race-detail.component.css']
})
export class RaceDetailComponent implements OnInit {

    loading = false;
    userRegister = false;
    isCameRace = false;
    userRaceStatus = 'race_did_not_start'; // race_did_not_start , race_start, race_is_over
    team = '';
    isCaptain = 0;
    nowHour = '';
    nowDate = '';
    calendarText = 'Takvime Ekle';
    isCalendar = true;
    localTimer = '00:00';
    startBtnText = 'KOŞUYA GELDİM';
    APIKEY = 'AIzaSyASgK-EyVswAr3dln86X7_LtpgWYN9Pf1c';
    origin = { latitude: 0, longitude: 0 };
    startDistanceInMeters = 0;
    endDistanceInMeters = 0;
    watch = null;
    race: Race;
    raceUser: RaceUser;
    subscription: Subscription;    

    constructor(
        private raceService: RaceService,
        private raceUserService: RaceUserService,
        private route: ActivatedRoute,
        private routerExtension: RouterExtensions,
        private activeRoute: ActivatedRoute,
    ) {
        let trLocale = require('moment/locale/tr');
        moment.locale('tr', trLocale);
    }

    ngOnInit(): void {
        this.race = new Race();
        this.raceUser = new RaceUser();
        this.subscription = this.route.params.subscribe(params => {
            const race_id = +params['id'];
            this.loadRace(race_id);
        });
    }

    loadRace(race_id: number) {
        this.loading = true;
        this.raceService.getRace(race_id)
            .subscribe((result) => {
                this.race = result[0];
                this.loadRaceUser(race_id);
            }, (error) => {
                console.log(error);
            });
    }

    loadRaceUser(race_id: number) {
        this.raceUserService.getRaceUser(race_id)
            .subscribe((result) => {
                this.raceUser = result[0];
                this.nowHour = moment(String(new Date())).format('HH:mm:ss');
                this.nowDate = moment(String(new Date())).format('Y-MM-DD');

                if (result != '') {
                    this.userRegister = true;

                    if (this.raceUser.start_date) {
                        this.isCameRace = true;
                        
                        if (new Date(this.nowDate + 'T' + this.nowHour) < new Date(this.race.start_date + 'T' + this.race.start_hour)) {
                            this.userRaceStatus = 'race_did_not_start';
                        } else {
                            this.userRaceStatus = 'race_start';
                        }
                    } else {
                        this.userRaceStatus = 'race_did_not_start';
                    }

                    if (this.raceUser.end_date) {
                        this.userRaceStatus = 'race_is_over';
                    }

                    if(this.raceUser.is_calendar == 1)
                    {
                        this.calendarText = 'Takvime Eklendi';
                        this.isCalendar = false;
                    }else {
                        this.calendarText = 'Takvime Ekle';
                        this.isCalendar = true;
                    }

                } else {
                    this.userRegister = false
                }

                this.loading = false;

            }, (error) => {
                console.log(error);
            })
    }

    raceUserRegister() {
        this.raceUserService.raceUserRegister(this.race.id)
            .subscribe((result) => {
                if (result['status']) {
                    this.routerExtension.navigate(['race-joined/' + this.race.id], { relativeTo: this.activeRoute.parent });
                } else {
                    dialogs.alert({
                        title: 'Uyarı',
                        message: result['message'],
                        okButtonText: "Tamam",
                        cancelable: false
                    });
                }
            },(error) => {
                console.log(error);
            });
    }

    locationStart() {
        this.startBtnText = 'Bekleyiniz...';
        geolocation.enableLocationRequest(true, true).then(() => {
            geolocation.getCurrentLocation({
                desiredAccuracy: Accuracy.high,
                maximumAge: 1000,
                timeout: 20000
            })
            .then(res => {
                this.origin.latitude = res.latitude;
                this.origin.longitude = res.longitude;

                let distanceMatrixAPIURL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&";
                distanceMatrixAPIURL += `origins=${this.origin.latitude},${this.origin.longitude}&destinations=${this.race.start_latitude},${this.race.start_longitude}&mode=walking&language=tr-TR&key=${this.APIKEY}`;

                http.getJSON(distanceMatrixAPIURL).then(
                    result => {
                        this.startDistanceInMeters = result['rows'][0].elements[0].distance.value
                        console.log('Başlangıç yerine uzaklık mesafesi: ' + result['rows'][0].elements[0].distance.text)
                    
                        if (this.startDistanceInMeters < 50) 
                        {
                            this.raceUserStart(1)
                        }
                        else
                        {
                            this.raceUserStart(0)
                        }
                    },
                    error => {
                        console.log(error);
                    }
                );
            })
            .catch(e => {
                console.log("error", e);
            });
        }, (e) => {
            console.log("Error: " + (e.message || e));
        }).catch(ex => {
            console.log("Unable to Enable Location", ex);
        });
    }

    raceUserStart(status: number) {
        if(status == 1) {
            this.raceUserService.raceUserStart(this.race.id)
                .subscribe((result) => {
                    if (result['status']) {
                        localStorage.setItem('race_detail', JSON.stringify(this.race))
                        dialogs.alert({
                            title: 'Başarılı',
                            message: result['message'],
                            okButtonText: 'Tamam',
                            cancelable: false 
                        }).then(() => {
                            this.loadRaceUser(this.race.id);
                        });
                    } else {
                        this.startBtnText = 'KOŞUYA GELDİM'
                        dialogs.alert({
                            title: 'Uyarı',
                            message: 'Koşuya geldin kaydı daha önce yapıldı.',
                            okButtonText: "Tamam",
                            cancelable: false
                        });
                    }
                },(error) => {
                    console.log(error);
                });
        }
        else
        {
            this.startBtnText = 'KOŞUYA GELDİM'

            dialogs.alert({
                title: 'Uyarı',
                message: 'Koşu alanınında değilsiniz',
                okButtonText: "Tamam",
                cancelable: false
            });
        }
    }

    registeredTeams() {
        // registered teams
    }
    
    teamChoose() {
        // team choose
    }

    addToCalendar() {
        if (this.isCalendar) {
            var options = {
                title: this.race.title,
                startDate: new Date(moment(String(this.race.start_date + ' ' + this.race.start_hour)).toString()),
                endDate: new Date(moment(String(this.race.start_date + ' ' + this.race.end_hour)).toString()),
                notes: this.race.description,
                location: this.race.open_address,
                url: 'http://www.aakosu.org',
                calendar: {
                    name: 'Adım Adım Koşu Takvimi',
                    color: '#FF0000',
                    accountName: 'AA Koşu'
                }
            }
    
            dialogs.confirm({
                title: this.race.title,
                message: 'Bu koşuyu takvime eklemek istiyor musunuz',
                okButtonText: "Ekle",
                cancelButtonText: "Vazgeç"
            }).then(result => {
                if(result) {
                    this.raceUserService.raceUserAddCalendar(this.race.id)
                        .subscribe((result) => {
                            if (result['status']) {
                                Calendar.createEvent(options).then(
                                    function(createdId) {
                                        console.log("Event ID: " + createdId)
                                        dialogs.alert({
                                            title: 'Başarılı',
                                            message: result['message'],
                                            okButtonText: 'Tamam',
                                            cancelable: true
                                        });
                                    },
                                    function(error) {
                                        console.log("Error creating an Event: " + error)
                                    }
                                )
    
                                this.calendarText = 'Takvime Eklendi'
                            } else {
                                dialogs.alert({
                                    title: 'Uyarı',
                                    message: result['message'],
                                    okButtonText: 'Tamam',
                                    cancelable: false 
                                });
                            }
                        },(error) => {
                            console.log(error);
                        });
                }
            });
        }
    }

    openMapURL(lat: string = '', lng: string = '') {
        if (lat == null) {
            lat = '';
        }

        if (lng == null) {
            lng = '';
        }

        let value = lat + ', ' + lng;
        let encodeUrl = ''

        if (platform.isIOS) {
            encodeUrl = encodeURI('http://maps.apple.com/?q=' + value)
        } else {
            encodeUrl = encodeURI('https://www.google.com/maps/search/?api=1&query=' + value)
        }

        utils.openUrl(encodeUrl)
    }

    dayAndMonthConvert(value: Date) {
        if (value) {
            return moment(String(value)).format('DD MMMM')
        }
    }

    dayConvert(value: Date) {
        if (value) {
            return moment(String(value)).format('dddd')
        }
    }

    timeConvert(value: Date){
        if (value) {
            return moment(String(value)).format('HH:mm')
        }
    }

    goRaceList() {
        this.routerExtension.navigate(['race-list'], { relativeTo: this.activeRoute.parent });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
