import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { NativeScriptUIListViewModule } from 'nativescript-ui-listview/angular';

import { RaceListComponent } from './race-list/race-list.component';
import { RaceDetailComponent } from './race-detail/race-detail.component';
import { RaceJoinedComponent } from './race-joined/race-joined.component';
import { RaceInviteFriendComponent } from './race-invite-friend/race-invite-friend.component';
import { RaceRegisteredUsersComponent } from './race-registered-users/race-registered-users.component';

@NgModule({
    declarations: [
        RaceListComponent,
        RaceDetailComponent,
        RaceJoinedComponent,
        RaceInviteFriendComponent,
        RaceRegisteredUsersComponent,
    ],
    imports: [
        NativeScriptCommonModule,
        NativeScriptUIListViewModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forChild([
            { path: '', redirectTo: 'race-list', pathMatch: 'full' },
            { path: 'race-list', component: RaceListComponent },
            { path: 'race-detail/:id', component: RaceDetailComponent },
            { path: 'race-joined/:id', component: RaceJoinedComponent },
            { path: 'race-invite-friend/:id', component: RaceInviteFriendComponent },
            { path: 'race-registered-users/:id', component: RaceRegisteredUsersComponent },
        ]),
        TNSFontIconModule.forRoot({
			'la': require('~/assets/line-awesome.min.css'),
        }),
    ],
    providers: [
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class RaceModule { }
