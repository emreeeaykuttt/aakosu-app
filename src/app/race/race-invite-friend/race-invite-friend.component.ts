import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { Subscription } from 'rxjs';
import { ObservableArray } from 'tns-core-modules/data/observable-array';
import { RadListViewComponent } from 'nativescript-ui-listview/angular';
import { ListViewEventData } from 'nativescript-ui-listview';
import { EventData } from 'tns-core-modules/data/observable';
import { TextView } from 'tns-core-modules/ui/text-view';
import * as dialogs from 'tns-core-modules/ui/dialogs';

import { UserService } from '~/app/services/user.service';
import { RaceUserService } from '../../services/race-user.service';
import { User } from '~/app/models/user.model';
import { RaceUser } from '../../models/race-user.model';

@Component({
    selector: 'RaceInviteFriend',
    moduleId: module.id,
    templateUrl: './race-invite-friend.component.html',
    styleUrls: ['./race-invite-friend.component.css']
})
export class RaceInviteFriendComponent implements OnInit {

    loading = false;
    userIdArray = [];
    warningText = 'Yukarıdaki alana davet etmek istediğin kişinin adını yazınız';
    raceId: number;
    user: User;
    raceUser: RaceUser;
    subscription: Subscription;  
    _friendItems: ObservableArray<User> = new ObservableArray([]); 
    @ViewChild('friendList', { static: false }) friendList: RadListViewComponent;

    constructor(
        private userService: UserService,
        private raceUserService: RaceUserService,
        private route: ActivatedRoute,
        private routerExtension: RouterExtensions,
    ) { }

    public get friendItems(): ObservableArray<User> {
        return this._friendItems;
    }

    ngOnInit(): void {
        this.subscription = this.route.params.subscribe(params => {
            this.raceId = +params['id'];
        });

        this.loadUserList();
    }

    onNavBtnTap(){
        this.routerExtension.navigate(['race-detail/' + this.raceId], { relativeTo: this.route.parent });
    }

    loadUserList(runLoading: boolean = true, searchText: string = '') {
        this.loading = runLoading;
        this.userService.getRaceUnregisteredUsers(this.raceId, searchText)
            .subscribe((result) => {
                this._friendItems = new ObservableArray([]);
                if (result != '') {
                    this.warningText = '';
                    this._friendItems.push(result);
                } else {
                    this.warningText = 'Aradığınız kullanıcı bulunamadı';
                }
                this.loading = false;
            }, (error) => {
                console.log(error);
            });
    }

    onPullToRefreshInitiated(args: ListViewEventData) {
        const that = new WeakRef(this);
        
        setTimeout(function () {
            that.get()._friendItems = new ObservableArray([])
            that.get().loadUserList(false);
            const listView = args.object;
            listView.notifyPullToRefreshFinished();
        }, 1000);
    }

    userSearch(args: EventData) {
        const searchText = args.object as TextView;
        this.loadUserList(true, searchText.text);
    }

    onItemSelected(args: ListViewEventData) {
        let itemSelected = this._friendItems.getItem(args.index);
        this.userIdArray.push(itemSelected.id)
    }

    onItemDeselected(args: ListViewEventData) {
        let itemSelected = this._friendItems.getItem(args.index);
        let itemVal = this.userIdArray.indexOf(itemSelected.id);
        this.userIdArray.splice(itemVal, 1);
    }

    sendInvitation() {
        let raceUsers = this.userIdArray;
        if (raceUsers.length > 0) {
            this.raceUserService.raceUserFriendInvite(this.raceId, raceUsers)
                .subscribe((result) => {
                    if (result['status']) {
                        dialogs.alert({
                            title: 'Başarılı',
                            message: result['message'],
                            okButtonText: 'Tamam',
                            cancelable: false 
                        }).then(() => {
                            this.routerExtension.navigate(['race-detail/' + this.raceId], { relativeTo: this.route.parent });
                        });
                    } else {
                        dialogs.alert({
                            title: 'Uyarı',
                            message: result['message'],
                            okButtonText: 'Tamam',
                            cancelable: false 
                        });
                    }
                },(error) => {
                    console.log(error);
                });
        } else {
            dialogs.alert({
                title: 'Uyarı',
                message: 'En az bir kişiye koşuya davet etmelisiniz!',
                okButtonText: "Tamam",
                cancelable: false
            });
        }
    }
}
