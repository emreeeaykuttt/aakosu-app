import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { Subscription } from 'rxjs';

import { UserService } from '../../services/user.service';
import { User } from '~/app/models/user.model';

@Component({
    selector: 'RaceJoined',
    moduleId: module.id,
    templateUrl: './race-joined.component.html',
    styleUrls: ['./race-joined.component.css']
})
export class RaceJoinedComponent implements OnInit {

    loading = false;
    raceId: number;
    user: User;
    subscription: Subscription;  

    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private activeRoute: ActivatedRoute,
        private routerExtension: RouterExtensions,
    ) { 
        this.user = new User();
    }

    ngOnInit(): void {
        this.subscription = this.route.params.subscribe(params => {
            this.raceId = +params['id'];
        });

        this.loadUser();
    }

    onNavBtnTap(){
        this.routerExtension.navigate(['race-detail/' + this.raceId], { relativeTo: this.activeRoute.parent });
    }

    loadUser() {
        this.loading = true;
        this.userService.getUser()
            .subscribe((result) => {
                this.user = result['items'][0];
                this.loading = false;
            }, (error) => {
                console.log(error);
            });
    }

    goRaceDetail() {
        this.routerExtension.navigate(['race-detail/' + this.raceId], { relativeTo: this.activeRoute.parent });
    }

}
