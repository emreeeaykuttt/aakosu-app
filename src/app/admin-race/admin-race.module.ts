import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { NativeScriptUIListViewModule } from 'nativescript-ui-listview/angular';
import { NativeScriptFormsModule } from "nativescript-angular/forms"

import { AdminRaceListComponent } from './admin-race-list/admin-race-list.component';
import { AdminRaceDetailComponent } from './admin-race-detail/admin-race-detail.component';
import { AdminRaceSaveComponent } from './admin-race-save/admin-race-save.component';
import { AdminRaceResultComponent } from './admin-race-result/admin-race-result.component';

@NgModule({
    declarations: [AdminRaceListComponent, AdminRaceDetailComponent, AdminRaceSaveComponent, AdminRaceResultComponent],
    imports: [
        NativeScriptCommonModule,
        NativeScriptUIListViewModule,
        NativeScriptFormsModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forChild([
            { path: '', redirectTo: 'admin-race-list', pathMatch: 'full' },
            { path: 'admin-race-list', component: AdminRaceListComponent },
            { path: 'admin-race-detail/:id', component: AdminRaceDetailComponent },
            { path: 'admin-race-save/:id', component: AdminRaceSaveComponent },
            { path: 'admin-race-result/:id', component: AdminRaceResultComponent },
        ]),
        TNSFontIconModule.forRoot({
			'la': require('~/assets/line-awesome.min.css'),
        }),
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class AdminRaceModule { }
