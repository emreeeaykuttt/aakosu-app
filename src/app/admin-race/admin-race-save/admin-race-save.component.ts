import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { Subscription } from 'rxjs';
import { SelectedIndexChangedEventData, ValueList } from 'nativescript-drop-down';
import * as dialogs from 'tns-core-modules/ui/dialogs';
import * as moment from 'moment';

import { RaceService } from '../../services/race.service';
import { TrailService } from '~/app/services/trail.service';
import { Race } from '../../models/race.model';

@Component({
    selector: 'AdminRaceSave',
    moduleId: module.id,
    templateUrl: './admin-race-save.component.html',
    styleUrls: ['./admin-race-save.component.css']
})
export class AdminRaceSaveComponent implements OnInit {

    loading = false;
    pageTitle = '';
    selectedTrail = 0;
    selectedRaceType = 0;
    trails: ValueList<string>;
    raceTypes: ValueList<string>;
    race: Race;
    subscription: Subscription;
    @ViewChild('errorTitle', { static: false }) errorTitle: ElementRef;
    @ViewChild('errorTrailID', { static: false }) errorTrailID: ElementRef;
    @ViewChild('errorStartDate', { static: false }) errorStartDate: ElementRef;
    @ViewChild('errorRaceType', { static: false }) errorRaceType: ElementRef;
    @ViewChild('errorStartHour', { static: false }) errorStartHour: ElementRef;
    @ViewChild('errorEndHour', { static: false }) errorEndHour: ElementRef;
    @ViewChild('errorMinTime', { static: false }) errorMinTime: ElementRef;
    @ViewChild('errorDescription', { static: false }) errorDescription: ElementRef;

    constructor(
        private raceService: RaceService,
        private trailService: TrailService,
        private route: ActivatedRoute,
        private routerExtension: RouterExtensions,
        private changeDetectorRef: ChangeDetectorRef
    ) {
        this.race = new Race();
    }

    ngOnInit(): void {
        this.raceTypes = new ValueList<string>([
            {value: '0', display: 'Seçiniz...'},
            {value: 'single', display: 'Tekli Koşu'},
            {value: 'team', display: 'Takımlı Koşu'},
        ]);

        this.subscription = this.route.params.subscribe(params => {
            const race_id = +params['id'];
            this.loadRace(race_id);
        });
    }

    onNavBtnTap(){
        if (this.race.id && this.race.id != 0) {
            this.routerExtension.navigate(['admin-race-detail', this.race.id], { relativeTo: this.route.parent });
        } else {
            this.routerExtension.navigate(['admin-race-list'], { relativeTo: this.route.parent });
        }
    }

    loadRace(race_id: number) {
        this.loading = true;
        this.raceService.getAdminRace(race_id)
            .subscribe((result) => {
                if (result[0]) {
                    this.race = result[0];
                    this.pageTitle = 'Koşu Düzenleme';
                    this.selectedRaceType = this.raceTypes.getIndex(this.race.race_type);
                    this.loadTrails(this.race.trail_id);
                } else {
                    this.pageTitle = 'Yeni Koşu Planla';
                    this.selectedRaceType = 0;
                    this.loadTrails(0);
                }
                this.changeDetectorRef.detectChanges();
                this.loading = false;
            }, (error) => {
                console.log(error);
            });
    }

    saveRace() {
        let validate = this.validateRace();

        if(validate) {
            this.loading = true;
            this.raceService.save(this.race)
                .subscribe((result) => {
                    let status = result['status'];
                    let message = result['data'];
                    let id = result['id'] ?  result['id'] : this.race.id;
                    dialogs.alert({
                        title: (status ? 'Başarılı' : 'Uyarı'),
                        message: message,
                        okButtonText: 'Tamam',
                        cancelable: false 
                    }).then(() => {
                        if (id) {
                            this.routerExtension.navigate(['admin-race-detail', id], { relativeTo: this.route.parent });
                        } else {
                            this.routerExtension.navigate(['admin-race-list'], { relativeTo: this.route.parent });
                        }
                    });
                    this.loading = false;
                }, (error) => {
                    console.log(error);
                });
        } else{
            dialogs.alert({
                title: 'Uyarı',
                message: 'Gerekli alanları doldurunuz!',
                okButtonText: 'Tamam',
                cancelable: false 
            });
        }
    }

    validateRace() {

        let status = true;
        let errorMessage = {
            title: this.errorTitle.nativeElement,
            trailID: this.errorTrailID.nativeElement,
            startDate: this.errorStartDate.nativeElement,
            raceType: this.errorRaceType.nativeElement,
            startHour: this.errorStartHour.nativeElement,
            endHour: this.errorEndHour.nativeElement,
            minTime: this.errorMinTime.nativeElement,
            description: this.errorDescription.nativeElement,
        }

        if(!this.race.title.trim()) {
            errorMessage.title.visibility = 'visible';
            errorMessage.title.text = '*Koşu Adı alanı boş bırakılamaz';
            status = false;
        } else {
            errorMessage.title.visibility = 'collapse';
            errorMessage.title.text = '';
        }

        if(this.race.trail_id == 0) {
            errorMessage.trailID.visibility = 'visible';
            errorMessage.trailID.text = '*Parkur seçiniz';
            status = false;
        } else {
            errorMessage.trailID.visibility = 'collapse';
            errorMessage.trailID.text = '';
        }

        if(!this.race.start_date) {
            errorMessage.startDate.visibility = 'visible';
            errorMessage.startDate.text = '*Tarih alanı boş bırakılamaz';
            status = false;
        } else {
            errorMessage.startDate.visibility = 'collapse';
            errorMessage.startDate.text = '';
        }

        if(this.race.race_type == '0' || !this.race.race_type) {
            errorMessage.raceType.visibility = 'visible';
            errorMessage.raceType.text = '*Koşu Tipi seçiniz';
            status = false;
        } else {
            errorMessage.raceType.visibility = 'collapse';
            errorMessage.raceType.text = '';
        }

        if(!this.race.start_hour) {
            errorMessage.startHour.visibility = 'visible';
            errorMessage.startHour.text = '*Başlangıç Saati Seçiniz';
            status = false;
        } else {
            errorMessage.startHour.visibility = 'collapse';
            errorMessage.startHour.text = '';
        }

        if(!this.race.end_hour) {
            errorMessage.endHour.visibility = 'visible';
            errorMessage.endHour.text = '*Bitiş Saati Seçiniz';
            status = false;
        } else {
            errorMessage.endHour.visibility = 'collapse';
            errorMessage.endHour.text = '';
        }
        
        if(!this.race.min_time) {
            errorMessage.minTime.visibility = 'visible';
            errorMessage.minTime.text = '*Min Süre Seçiniz';
            status = false;
        } else {
            errorMessage.minTime.visibility = 'collapse';
            errorMessage.minTime.text = '';
        }

        if(!this.race.description.trim()) {
            errorMessage.description.visibility = 'visible';
            errorMessage.description.text = '*Koşu Hakkında alanı boş bırakılamaz';
            status = false;
        } else {
            errorMessage.description.visibility = 'collapse';
            errorMessage.description.text = '';
        }

        return status;
    }

    loadTrails(trail_id: number) {
        this.trailService.getAdminTrails()
            .subscribe((result) => {
                let keys = Object.keys(result);
                let trails = [];

                trails.push({value: '0', display: 'Seçiniz...'});
                for (let loop = 0; loop < keys.length; loop++) {
                    trails.push({
                        value: `${result[loop].id}`,
                        display: result[loop].track_name
                    });
                }
                
                this.trails = new ValueList<string>(trails);
                this.selectedTrail = this.trails.getIndex(`${trail_id}`);
                this.race.trail_id = trail_id;
            }, (error) => {
                console.log(error);
            });
    }

    trailChange(args: SelectedIndexChangedEventData) {
        let trail_id = Number(this.trails.getValue(args.newIndex));
        this.race.trail_id = trail_id;
    }

    raceTypeChange(args: SelectedIndexChangedEventData) {
        this.race.race_type = this.raceTypes.getValue(args.newIndex);
    }

    startDateChange(args) {
        if(args.value != 'Invalid Date') {
            this.race.start_date = this.getFormattedDate(args.value)
        }
    }

    getFormattedDate(date: Date) {
        return date.getFullYear() + '-' + ('0' + (date.getMonth()+1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2)
    }

    startHourChange(args) {
        if(args.value != 'Invalid Date') {
            this.race.start_hour = this.getFormattedTime(args.value)
        }
    }

    endHourChange(args) {
        if(args.value != 'Invalid Date') {
            this.race.end_hour = this.getFormattedTime(args.value)
        }
    }

    getFormattedTime(date: Date) {
        const h = date.getHours();
        const m = date.getMinutes();
        return (h < 10 ? '0' : '') + h + ':' + (m < 10 ? '0' : '') + m;
    }

}
