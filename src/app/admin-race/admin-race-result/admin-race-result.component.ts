import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { Subscription } from 'rxjs';
import { ObservableArray } from 'tns-core-modules/data/observable-array';
import { RadListViewComponent } from 'nativescript-ui-listview/angular';
import { ListViewEventData } from 'nativescript-ui-listview';

import { RaceService } from '~/app/services/race.service';
import { RaceUserService } from '~/app/services/race-user.service';
import { Race } from '~/app/models/race.model';
import { RaceUser } from '~/app/models/race-user.model';

@Component({
    selector: 'AdminRaceResult',
    moduleId: module.id,
    templateUrl: './admin-race-result.component.html',
    styleUrls: ['./admin-race-result.component.css']
})
export class AdminRaceResultComponent implements OnInit {

    loading = false;
    tab1 = true;
    tab2 = false;
    tab3 = false;
    tab4 = false;
    tab5 = false;
    race: Race;
    raceUser: RaceUser;
    subscription: Subscription;
    _allRuns: ObservableArray<RaceUser> = new ObservableArray([]);
    _femaleRuns: ObservableArray<RaceUser> = new ObservableArray([]);
    _maleRuns: ObservableArray<RaceUser> = new ObservableArray([]);
    _otherRuns: ObservableArray<RaceUser> = new ObservableArray([]);
    _teamRuns: ObservableArray<RaceUser> = new ObservableArray([]);
    @ViewChild('allRunsView', { static: false }) allRunsView: RadListViewComponent;
    @ViewChild('femaleRunsView', { static: false }) femaleRunsView: RadListViewComponent;
    @ViewChild('maleRunsView', { static: false }) maleRunsView: RadListViewComponent;
    @ViewChild('otherRunsView', { static: false }) otherRunsView: RadListViewComponent;
    @ViewChild('teamRunsView', { static: false }) teamRunsView: RadListViewComponent;

    constructor(
        private raceService: RaceService,
        private raceUserService: RaceUserService,
        private route: ActivatedRoute,
        private routerExtension: RouterExtensions,
        private changeDetectorRef: ChangeDetectorRef
    ) {
        this.race = new Race();
        this.raceUser = new RaceUser();
    }

    public get allRuns(): ObservableArray<RaceUser> {
        return this._allRuns;
    }

    public get femaleRuns(): ObservableArray<RaceUser> {
        return this._femaleRuns;
    }

    public get maleRuns(): ObservableArray<RaceUser> {
        return this._maleRuns;
    }

    public get otherRuns(): ObservableArray<RaceUser> {
        return this._otherRuns;
    }

    public get teamRuns(): ObservableArray<RaceUser> {
        return this._teamRuns;
    }

    ngOnInit(): void {
        this.subscription = this.route.params.subscribe(params => {
            const race_id = +params['id'];
            this.race.id = race_id;
            this.loadRace(race_id);
            this.loadGenderRuns(race_id, '');
            this.loadGenderRuns(race_id, 'kadin');
            this.loadGenderRuns(race_id, 'erkek');
            this.loadGenderRuns(race_id, 'diger');
            this.loadTeamRuns(race_id);
        });
        this.changeDetectorRef.detectChanges();
    }

    onNavBtnTap(){
        this.routerExtension.navigate(['admin-race-detail', this.race.id], { relativeTo: this.route.parent });
    }

    loadRace(race_id: number) {
        this.loading = true;
        this.raceService.getAdminRace(race_id)
            .subscribe((result) => {
                this.race = result[0];
            }, (error) => {
                console.log(error);
            });
    }

    loadGenderRuns(race_id: number, gender_val: string, runLoading: boolean = true) {
        this.loading = runLoading;
        this.raceUserService.getAdminRegisteredRaceUsers(race_id, gender_val)
            .subscribe((result) => {
                if (result != '') {
                    if (gender_val == '') {
                        this._allRuns.push(result);
                    } else if (gender_val == 'kadin') {
                        this._femaleRuns.push(result);
                    } else if (gender_val == 'erkek') {
                        this._maleRuns.push(result);
                    } else if (gender_val == 'diger') {
                        this._otherRuns.push(result);
                    }
                }
            })
    }

    refreshGenderRuns(args: ListViewEventData, gender_val:string) {
        const that = new WeakRef(this);
        
        setTimeout(function () {
            if (gender_val = '') {
                that.get()._allRuns = new ObservableArray([])
            } else if (gender_val == 'kadin') {
                that.get()._femaleRuns = new ObservableArray([])
            } else if (gender_val == 'erkek') {
                that.get()._maleRuns = new ObservableArray([])
            } else if (gender_val == 'diger') {
                that.get()._otherRuns = new ObservableArray([])
            }

            that.get().loadGenderRuns(that.get().race.id, gender_val, false);
            const listView = args.object;
            listView.notifyPullToRefreshFinished();
        }, 1000);
    }

    loadTeamRuns(race_id: number, runLoading: boolean = true) {
        this.loading = runLoading;
        this.raceUserService.getAdminRegisteredTeams(race_id)
            .subscribe((result) => {
                if (result != '') {
                    this._teamRuns.push(result);
                }
                this.loading = false;
            })
    }

    refreshTeamRuns(args: ListViewEventData) {
        const that = new WeakRef(this);
        
        setTimeout(function () {
            that.get()._teamRuns = new ObservableArray([])
            that.get().loadTeamRuns(that.get().race.id, false);
            const listView = args.object;
            listView.notifyPullToRefreshFinished();
        }, 1000);
    }

    timeEdit(user_id: number, finishing_hour: string, fullname: string) {
        console.log('user_id: ' + user_id);
        console.log('user_id: ' + finishing_hour);
        console.log('user_id: ' + fullname);
    }

    tabControl(val: string) {
        if (val == 'all') {
            this.tab1 = true;
            this.tab2 = false;
            this.tab3 = false;
            this.tab4 = false;
            this.tab5 = false;
        } else if (val == 'female') {
            this.tab1 = false;
            this.tab2 = true;
            this.tab3 = false;
            this.tab4 = false;
            this.tab5 = false;
        } else if (val == 'male') {
            this.tab1 = false;
            this.tab2 = false;
            this.tab3 = true;
            this.tab4 = false;
            this.tab5 = false;
        } else if (val == 'other') {
            this.tab1 = false;
            this.tab2 = false;
            this.tab3 = false;
            this.tab4 = true;
            this.tab5 = false;
        } else if (val == 'team') {
            this.tab1 = false;
            this.tab2 = false;
            this.tab3 = false;
            this.tab4 = false;
            this.tab5 = true;
        }
    }
}
