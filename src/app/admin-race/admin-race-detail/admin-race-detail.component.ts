import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { Subscription } from 'rxjs';
import * as platform from 'tns-core-modules/platform';
import * as utils from 'tns-core-modules/utils/utils';
import * as dialogs from 'tns-core-modules/ui/dialogs';
import * as moment from 'moment';

import { RaceService } from '../../services/race.service';
import { Race } from '../../models/race.model';

@Component({
    selector: 'AdminRaceDetail',
    moduleId: module.id,
    templateUrl: './admin-race-detail.component.html',
    styleUrls: ['./admin-race-detail.component.css']
})
export class AdminRaceDetailComponent implements OnInit {

    loading = false;
    nowHour = moment(String(new Date())).format('HH:mm:ss');
    nowDate = moment(String(new Date())).format('YYYY-MM-DD');
    raceStatus = 'race_did_not_start'; // race_did_not_start , race_start, race_is_over
    race: Race;
    subscription: Subscription;    

    constructor(
        private raceService: RaceService,
        private route: ActivatedRoute,
        private routerExtension: RouterExtensions,
        private activeRoute: ActivatedRoute,
    ) {
        let trLocale = require('moment/locale/tr');
        moment.locale('tr', trLocale);
    }

    ngOnInit(): void {
        this.race = new Race();
        this.subscription = this.route.params.subscribe(params => {
            const race_id = +params['id'];
            this.loadRace(race_id);
        });
    }

    loadRace(race_id: number) {
        this.loading = true;
        this.raceService.getAdminRace(race_id)
            .subscribe((result) => {
                this.race = result[0];
                this.nowHour = moment(String(new Date())).format('HH:mm:ss');
                this.nowDate = moment(String(new Date())).format('Y-MM-DD');
                if (new Date(this.nowDate + 'T' + this.nowHour) < new Date(this.race.start_date + 'T' + this.race.start_hour)) {
                    this.raceStatus = 'race_did_not_start';
                } else if (new Date(this.nowDate + 'T' + this.nowHour) >= new Date(this.race.start_date + 'T' + this.race.start_hour) && new Date(this.nowDate + 'T' + this.nowHour) <= new Date(this.race.start_date + 'T' + this.race.end_hour)) {
                    this.raceStatus = 'race_start';
                } else {
                    this.raceStatus = 'race_is_over';
                }
                this.loading = false;
            }, (error) => {
                console.log(error);
            });
    }

    deleteRace(race_id: number) {
        dialogs.confirm({
            title: "Koşuyu silmek istediğinize",
            message: "Emin misiniz",
            okButtonText: "Evet",
            cancelButtonText: "Vazgeç"
        }).then(result => {
            if (result) {
                dialogs.confirm({
                    title: "Koşuyu silerseniz koşuya kayıt olan kişileri ve varsa takımları silmiş olursunuz",
                    message: "Silmek istediğinize emin misiniz",
                    okButtonText: "Sil",
                    cancelButtonText: "Vazgeç"
                }).then(result => {
                    if (result) {
                        this.loading = true;
                        this.raceService.delete(race_id)
                            .subscribe((result) => {
                                let status = result['status'];
                                let message = result['data'];
                                dialogs.alert({
                                    title: (status ? 'Başarılı' : 'Uyarı'),
                                    message: message,
                                    okButtonText: 'Tamam',
                                    cancelable: false 
                                }).then(() => {
                                    this.routerExtension.navigate(['admin-race-list'], { relativeTo: this.route.parent });
                                });
                                this.loading = false;
                            }, (error) => {
                                console.log(error);
                            });
                    }
                });
            }
        });
    }

    openMapURL(lat: string = '', lng: string = '') {
        if (lat == null) {
            lat = '';
        }

        if (lng == null) {
            lng = '';
        }

        let value = lat + ', ' + lng;
        let encodeUrl = ''

        if (platform.isIOS) {
            encodeUrl = encodeURI('http://maps.apple.com/?q=' + value)
        } else {
            encodeUrl = encodeURI('https://www.google.com/maps/search/?api=1&query=' + value)
        }

        utils.openUrl(encodeUrl)
    }

    dayAndMonthConvert(value: Date) {
        if (value) {
            return moment(String(value)).format('DD MMMM')
        }
    }

    dayConvert(value: Date) {
        if (value) {
            return moment(String(value)).format('dddd')
        }
    }

    timeConvert(value: Date) {
        if (value) {
            return moment(String(value)).format('HH:mm')
        }
    }

    timeConvertSecond(date) {
        let convertDate = moment(String(date)).format('YYYY/MM/DD HH:mm:ss')
        let datetimeNow = new Date(convertDate).getTime() 
        
        return datetimeNow;
    }

    goRaceList() {
        this.routerExtension.navigate(['admin-race-list'], { relativeTo: this.activeRoute.parent });
    }

}
