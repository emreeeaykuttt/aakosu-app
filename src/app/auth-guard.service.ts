import { Injectable } from '@angular/core';
import { API } from './services/api';
import { RouterExtensions } from 'nativescript-angular/router';

@Injectable()
export class AuthGuard {
    constructor(private router: RouterExtensions) { }

    canActivate() {
        if (API.isLoggedIn()) {
            return true;
        } else {
            this.router.navigate(['/home'], { clearHistory:true });
            return false;
        }
    }
}