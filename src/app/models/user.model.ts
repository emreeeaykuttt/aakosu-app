const validator = require('email-validator');

export class User {
    constructor(
        public id?: number,
        public device_token?: string,
        public firstname?: string,
        public lastname?: string,
        public email?: string,
        public password?: string,
        public password_again?: string,
        public passphoto_url?: string,
        public gender?: string,
        public telephone?: string,
        public national_id?: string,
        public birthdate?: string,
        public country_id?: string,
        public city_id?: number,
        public district_id?: number,
        public street?: string,
        public title?: string,
        public organisation?: string,
        public height_cm?: number,
        public weight_kg?: number,
        public longest_run_km?: string,
        public website_url?: string,
        public facebook_url?: string,
        public twitter_url?: string,
        public linkedin_username?: string,
        public user_info_text?: string,
        public inform_rookies?: boolean,
        public inform_activity?: boolean,
        public inform_corporate_activity?: boolean,
        public be_volunteer?: boolean,
        public is_contract?: boolean,
        public other_sports_text?: string,
        public created_at?: Date,
        public updated_at?: Date) {
        // id;
        // firstname;
        // lastname;
        // email;
        // password;
        // password_again;
        // passphoto_url;
        // gender;
        // telephone;
        // national_id;
        // birthdate;
        // country_id;
        // city_id;
        // district_id;
        // street;
        // title;
        // organisation;
        // height_cm;
        // weight_kg;
        // longest_run_km;
        // website_url;
        // facebook_url;
        // twitter_url;
        // linkedin_username;
        // user_info_text;
        // inform_rookies;
        // inform_activity;
        // inform_corporate_activity;
        // be_volunteer;
        // is_contract;
        // other_sports_text;
        // created_at;
        // updated_at;
    }
    
    // isValidEmail() {
    //     return validator.validate(this.email);
    // }
}