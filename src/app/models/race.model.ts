export class Race {
    constructor(
        public id?: number,
        public title?: string,
        public trail_id?: number,
        public track_id?: number,
        public start_date?: string,
        public race_type?: string,
        public start_hour?: string,
        public end_hour?: string,
        public min_time?: string,
        public max_time?: number,
        public description?: string,
        public completed?: number,
        public admin_group_id?: number,
        public user_id?: number,
        public created_at?: Date,
        public updated_at?: Date,
        public is_user?: number,
        public count_user?: number,
        public count_team?: number,
        public name?: string,
        public racecourse?: string,
        public city_id?: number,
        public city_name?: string,
        public open_address?: string,
        public start_latitude?: string,
        public start_longitude?: string,
        public end_latitude?: string,
        public end_longitude?: string,
        ) {

    }
}
