export class Notification {
    constructor(
        public id?: number,
        public sender_user_id?: number,
        public receiver_user_id?: number,
        public race_id?: number,
        public team_id?: number,
        public title?: string,
        public description?: string,
        public icon?: string,
        public function_name?: string,
        public viewed?: number,
        public opened?: number,
        public notification_sent?: number,
        public created_at?: Date,
        public updated_at?: Date) {

    }
}