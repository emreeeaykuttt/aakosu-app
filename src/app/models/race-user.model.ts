export class RaceUser {
    constructor(
        public id?: number,
        public race_id?: number,
        public user_team_id?: number,
        public user_id?: number,
        public start_date?: Date,
        public end_date?: Date,
        public ranking?: number,
        public finishing_time?: number,
        public is_calendar?: number,
        public time_result?: number,
        public finishing_hour?: string,
        public count_user?: number,
        public count_team?: number,
        public created_at?: Date,
        public updated_at?: Date) {
        id;
        race_id;
        user_team_id;
        user_id;
        start_date;
        end_date;
        ranking;
        finishing_time;
        is_calendar;
        time_result;
        finishing_hour;
        count_user;
        count_team;
        created_at;
        updated_at;
    }
}