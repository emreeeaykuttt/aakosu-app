export class Trail {
    constructor(
        public id?: number,
        public track_name?: string,
        public city?: string,
        public city_id?: number,
        public open_address?: string,
        public start_latitude?: string,
        public start_longitude?: string,
        public end_latitude?: string,
        public end_longitude?: string,
        public racecourse?: string,
        public user_id?: number,
        public admin_group_id?: number,
        public created_at?: Date,
        public updated_at?: Date) {
    }
}