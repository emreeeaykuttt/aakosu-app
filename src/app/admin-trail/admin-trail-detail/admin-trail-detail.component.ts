import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { Subscription } from 'rxjs';
import * as platform from 'tns-core-modules/platform';
import * as utils from 'tns-core-modules/utils/utils';
import * as dialogs from 'tns-core-modules/ui/dialogs';

import { TrailService } from '~/app/services/trail.service';
import { Trail } from '~/app/models/trail.model';

@Component({
    selector: 'AdminTrailDetail',
    moduleId: module.id,
    templateUrl: './admin-trail-detail.component.html',
    styleUrls: ['./admin-trail-detail.component.css']
})
export class AdminTrailDetailComponent implements OnInit {

    loading = false;
    trail: Trail;
    subscription: Subscription;

    constructor(
        private trailService: TrailService,
        private route: ActivatedRoute,
        private routerExtension: RouterExtensions,
    ) {
    }

    ngOnInit(): void {
        this.trail = new Trail();
        this.subscription = this.route.params.subscribe(params => {
            const trail_id = +params['id'];
            this.loadTrail(trail_id);
        });
    }

    loadTrail(trail_id: number) {
        this.loading = true;
        this.trailService.getAdminTrail(trail_id)
            .subscribe((result) => {
                this.trail = result;
                this.loading = false;
            }, (error) => {
                console.log(error);
            });
    }

    deleteTrail(trail_id: number) {
        dialogs.confirm({
            title: "Koşuyu silmek istediğinize",
            message: "Emin misiniz",
            okButtonText: "Evet",
            cancelButtonText: "Vazgeç"
        }).then(result => {
            if (result) {    
                this.loading = true;
                this.trailService.delete(trail_id)
                    .subscribe((result) => {
                        let status = result['status'];
                        let message = result['data'];
                        dialogs.alert({
                            title: (status ? 'Başarılı' : 'Uyarı'),
                            message: message,
                            okButtonText: 'Tamam',
                            cancelable: false 
                        }).then(() => {
                            this.routerExtension.navigate(['admin-trail-list'], { relativeTo: this.route.parent });
                        });
                        this.loading = false;
                    }, (error) => {
                        console.log(error);
                    });
            }
        });
    }

    openMapURL(lat: string = '', lng: string = '') {
        if (lat == null) {
            lat = '';
        }

        if (lng == null) {
            lng = '';
        }

        let value = lat + ', ' + lng;
        let encodeUrl = ''

        if (platform.isIOS) {
            encodeUrl = encodeURI('http://maps.apple.com/?q=' + value)
        } else {
            encodeUrl = encodeURI('https://www.google.com/maps/search/?api=1&query=' + value)
        }

        utils.openUrl(encodeUrl)
    }

    goTrailList() {
        this.routerExtension.navigate(['admin-trail-list'], { relativeTo: this.route.parent });
    }

}
