import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { Subscription } from 'rxjs';
import { SelectedIndexChangedEventData, ValueList } from 'nativescript-drop-down';
import * as dialogs from 'tns-core-modules/ui/dialogs';

import { TrailService } from '~/app/services/trail.service';
import { UserService } from '~/app/services/user.service';
import { Trail } from '~/app/models/trail.model';

@Component({
    selector: 'AdminTrailSave',
    moduleId: module.id,
    templateUrl: './admin-trail-save.component.html',
    styleUrls: ['./admin-trail-save.component.css']
})
export class AdminTrailSaveComponent implements OnInit {

    loading = false;
    pageTitle = '';
    selectedCity = 0;
    trail: Trail;
    subscription: Subscription;
    cities: ValueList<string>;
    @ViewChild('errorTrackName', { static: false }) errorTrackName: ElementRef;
    @ViewChild('errorRacecourse', { static: false }) errorRacecourse: ElementRef;
    @ViewChild('errorCityID', { static: false }) errorCityID: ElementRef;
    @ViewChild('errorStartLatitude', { static: false }) errorStartLatitude: ElementRef;
    @ViewChild('errorStartLongitude', { static: false }) errorStartLongitude: ElementRef;
    @ViewChild('errorEndLatitude', { static: false }) errorEndLatitude: ElementRef;
    @ViewChild('errorEndLongitude', { static: false }) errorEndLongitude: ElementRef;
    @ViewChild('errorOpenAddress', { static: false }) errorOpenAddress: ElementRef;

    constructor(
        private trailService: TrailService,
        private userService: UserService,
        private route: ActivatedRoute,
        private routerExtension: RouterExtensions,
    ) {
    }

    ngOnInit(): void {
        this.trail = new Trail();
        this.subscription = this.route.params.subscribe(params => {
            const trail_id = +params['id'];
            this.loadTrail(trail_id);
        });
    }

    onNavBtnTap(){
        if (this.trail.id && this.trail.id != 0) {
            this.routerExtension.navigate(['admin-trail-detail', this.trail.id], { relativeTo: this.route.parent });
        } else {
            this.routerExtension.navigate(['admin-trail-list'], { relativeTo: this.route.parent });
        }
    }

    loadTrail(trail_id: number) {
        this.loading = true;
        this.trailService.getAdminTrail(trail_id)
            .subscribe((result) => {
                if (result) {
                    this.trail = result;
                    this.pageTitle = 'Parkur Düzenleme';
                    this.loadCities(this.trail.city_id);
                } else {
                    this.pageTitle = 'Parkur Ekle';
                    this.loadCities(0);
                }
                this.loading = false;
            }, (error) => {
                console.log(error);
            });
    }

    saveTrail() {
        let validate = this.validateTrail();

        if(validate) {
            this.loading = true;
            this.trailService.save(this.trail)
                .subscribe((result) => {
                    let status = result['status'];
                    let message = result['data'];
                    let id = result['id'] ?  result['id'] : this.trail.id;
                    dialogs.alert({
                        title: (status ? 'Başarılı' : 'Uyarı'),
                        message: message,
                        okButtonText: 'Tamam',
                        cancelable: false 
                    }).then(() => {
                        if (id) {
                            this.routerExtension.navigate(['admin-trail-detail', id], { relativeTo: this.route.parent });
                        } else {
                            this.routerExtension.navigate(['admin-trail-list'], { relativeTo: this.route.parent });
                        }
                    });
                    this.loading = false;
                }, (error) => {
                    console.log(error);
                });
        } else{
            dialogs.alert({
                title: 'Uyarı',
                message: 'Gerekli alanları doldurunuz!',
                okButtonText: 'Tamam',
                cancelable: false 
            });
        }
    }

    validateTrail() {

        let status = true;
        let errorMessage = {
            trackName: this.errorTrackName.nativeElement,
            racecourse: this.errorRacecourse.nativeElement,
            cityID: this.errorCityID.nativeElement,
            startLatitude: this.errorStartLatitude.nativeElement,
            startLongitude: this.errorStartLongitude.nativeElement,
            endLatitude: this.errorEndLatitude.nativeElement,
            endLongitude: this.errorEndLongitude.nativeElement,
            openAddress: this.errorOpenAddress.nativeElement,
        }

        if(!this.trail.track_name.trim()) {
            errorMessage.trackName.visibility = 'visible';
            errorMessage.trackName.text = '*Parkur Adı alanı boş bırakılamaz';
            status = false;
        } else {
            errorMessage.trackName.visibility = 'collapse';
            errorMessage.trackName.text = '';
        }

        if(!this.trail.racecourse.trim()) {
            errorMessage.racecourse.visibility = 'visible';
            errorMessage.racecourse.text = '*Yarış Pisti alanı boş bırakılamaz';
            status = false;
        } else {
            errorMessage.racecourse.visibility = 'collapse';
            errorMessage.racecourse.text = '';
        }

        if(this.trail.city_id == 0 || !this.trail.city_id) {
            errorMessage.cityID.visibility = 'visible';
            errorMessage.cityID.text = '*Şehir seçiniz';
            status = false;
        } else {
            errorMessage.cityID.visibility = 'collapse';
            errorMessage.cityID.text = '';
        }

        if(!this.trail.start_latitude.trim()) {
            errorMessage.startLatitude.visibility = 'visible';
            errorMessage.startLatitude.text = '*Lat alanı boş bırakılamaz';
            status = false;
        } else {
            errorMessage.startLatitude.visibility = 'collapse';
            errorMessage.startLatitude.text = '';
        }

        if(!this.trail.start_longitude.trim()) {
            errorMessage.startLongitude.visibility = 'visible';
            errorMessage.startLongitude.text = '*Lng alanı boş bırakılamaz';
            status = false;
        } else {
            errorMessage.startLongitude.visibility = 'collapse';
            errorMessage.startLongitude.text = '';
        }

        if(!this.trail.end_latitude.trim()) {
            errorMessage.endLatitude.visibility = 'visible';
            errorMessage.endLatitude.text = '*Lat alanı boş bırakılamaz';
            status = false;
        } else {
            errorMessage.endLatitude.visibility = 'collapse';
            errorMessage.endLatitude.text = '';
        }

        if(!this.trail.end_longitude.trim()) {
            errorMessage.endLongitude.visibility = 'visible';
            errorMessage.endLongitude.text = '*Lng alanı boş bırakılamaz';
            status = false;
        } else {
            errorMessage.endLongitude.visibility = 'collapse';
            errorMessage.endLongitude.text = '';
        }

        if(!this.trail.open_address.trim()) {
            errorMessage.openAddress.visibility = 'visible';
            errorMessage.openAddress.text = '*Açık Adres alanı boş bırakılamaz';
            status = false;
        } else {
            errorMessage.openAddress.visibility = 'collapse';
            errorMessage.openAddress.text = '';
        }

        return status;
    }

    loadCities(city_id: number) {
        this.userService.getCities()
            .subscribe((result) => {
                let keys = Object.keys(result);
                let cities = [];

                cities.push({value: '0', display: 'Seçiniz...'});
                for (let loop = 0; loop < keys.length; loop++) {
                    cities.push({
                        value: `${result[loop].city_code}`,
                        display: result[loop].city_name
                    });
                }

                this.cities = new ValueList<string>(cities);
                this.selectedCity = this.cities.getIndex(`${city_id}`)
            }, (error) => {
                console.log(error);
            });
    }

    cityChange(args: SelectedIndexChangedEventData) {
        let city_id = Number(this.cities.getValue(args.newIndex));
        this.trail.city_id = city_id;
    }
    
}
