import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ObservableArray } from 'tns-core-modules/data/observable-array';
import { RadListViewComponent } from 'nativescript-ui-listview/angular';
import { ListViewEventData } from 'nativescript-ui-listview';

import { TrailService } from '~/app/services/trail.service';
import { Trail } from '~/app/models/trail.model';

@Component({
    selector: 'AdminTrailList',
    templateUrl: './admin-trail-list.component.html',
    styleUrls: ['./admin-trail-list.component.css']
})
export class AdminTrailListComponent implements OnInit {

    loading = false;
    _trailItems: ObservableArray<Trail> = new ObservableArray([]); 
    @ViewChild('adminTrailList', { static: false }) adminTrailList: RadListViewComponent;

    constructor(private router: Router, private trailService: TrailService) {
    }

    public get trailItems(): ObservableArray<Trail> {
        return this._trailItems;
    }

    ngOnInit(): void {
        this.loadTrailList();
    }

    loadTrailList(runLoading: boolean = true) {
        this.loading = runLoading;
        this.trailService.getAdminTrails()
            .subscribe((result) => {
                this._trailItems.push(result);
                this.loading = false;
            }, (error) => {
                console.log(error);
            });
    }

    onPullToRefreshInitiated(args: ListViewEventData) {
        const that = new WeakRef(this);
        
        setTimeout(function () {
            that.get()._trailItems = new ObservableArray([])
            that.get().loadTrailList(false);
            const listView = args.object;
            listView.notifyPullToRefreshFinished();
        }, 1000);
    }

}
