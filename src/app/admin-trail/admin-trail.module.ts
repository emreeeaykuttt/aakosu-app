import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { NativeScriptUIListViewModule } from 'nativescript-ui-listview/angular';
import { NativeScriptFormsModule } from "nativescript-angular/forms"

import { AdminTrailListComponent } from './admin-trail-list/admin-trail-list.component';
import { AdminTrailDetailComponent } from './admin-trail-detail/admin-trail-detail.component';
import { AdminTrailSaveComponent } from './admin-trail-save/admin-trail-save.component';

@NgModule({
    declarations: [AdminTrailListComponent, AdminTrailDetailComponent, AdminTrailSaveComponent],
    imports: [
        NativeScriptCommonModule,
        NativeScriptUIListViewModule,
        NativeScriptFormsModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forChild([
            { path: '', redirectTo: 'admin-trail-list', pathMatch: 'full' },
            { path: 'admin-trail-list', component: AdminTrailListComponent },
            { path: 'admin-trail-detail/:id', component: AdminTrailDetailComponent },
            { path: 'admin-trail-save/:id', component: AdminTrailSaveComponent },
        ]),
        TNSFontIconModule.forRoot({
			'la': require('~/assets/line-awesome.min.css'),
        }),
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class AdminTrailModule { }
