import { Component, OnInit, ViewChild } from '@angular/core';
import { ObservableArray } from 'tns-core-modules/data/observable-array';
import { RadListViewComponent } from 'nativescript-ui-listview/angular';
import { ListViewEventData } from 'nativescript-ui-listview';
import * as moment from 'moment';

import { UserService } from '~/app/services/user.service';
import { RaceUserService } from '~/app/services/race-user.service';
import { RaceService } from '~/app/services/race.service';
import { User } from '~/app/models/user.model';
import { RaceUser } from '~/app/models/race-user.model';
import { Race } from '~/app/models/race.model';

@Component({
    selector: 'ResultList',
    templateUrl: './result-list.component.html',
    styleUrls: ['./result-list.component.css']
})
export class ResultListComponent implements OnInit {

    loading = false;
    tab1 = true;
    tab2 = false;
    user: User;
    raceUser: RaceUser;
    _userAttendedRuns: ObservableArray<RaceUser> = new ObservableArray([]); 
    _allRuns: ObservableArray<Race> = new ObservableArray([]); 
    @ViewChild('userAttendedRunsView', { static: false }) userAttendedRunsView: RadListViewComponent;
    @ViewChild('allRunsView', { static: false }) allRunsView: RadListViewComponent;

    constructor(private userService: UserService, private raceService: RaceService, private raceUserService: RaceUserService) {
        let trLocale = require('moment/locale/tr');
        moment.locale('tr', trLocale);
        this.user = new User(); 
        this.raceUser = new RaceUser();
    }

    public get userAttendedRuns(): ObservableArray<RaceUser> {
        return this._userAttendedRuns;
    }

    public get allRuns(): ObservableArray<Race> {
        return this._allRuns;
    }

    ngOnInit(): void {
        this.loadUser();
        this.userBestRun();
        this.loadUserAttendedRuns();
        this.loadAllRuns();
    }

    loadUser() {
        this.loading = true;
        this.userService.getUser()
            .subscribe((result) => {
                if (result != '') {
                    this.user = result['items'][0];
                }
            }, (error) => {
                console.log(error);
            });
    }

    userBestRun() {
        this.raceUserService.getUserBestRun()
            .subscribe((result) => {
                if (result != ''){
                    this.raceUser = result[0];
                }
            }, (error) => {
                console.log(error);
            });
    }

    loadUserAttendedRuns(runLoading: boolean = true) {
        this.loading = runLoading;
        this.raceUserService.getUserAttendedRuns()
            .subscribe((result) => {
                if (result != '') {
                    this._userAttendedRuns.push(result);
                }
                this.loading = false;
            })
    }

    refreshUserAttendedRuns(args: ListViewEventData) {
        const that = new WeakRef(this);
        
        setTimeout(function () {
            that.get()._userAttendedRuns = new ObservableArray([])
            that.get().loadUserAttendedRuns(false);
            const listView = args.object;
            listView.notifyPullToRefreshFinished();
        }, 1000);
    }

    loadAllRuns(runLoading: boolean = true) {
        this.loading = runLoading;
        this.raceService.getRaceResults()
            .subscribe((result) => {
                if (result != '') {
                    this._allRuns.push(result);
                }
                this.loading = false;
            })
    }

    refreshAllRuns(args: ListViewEventData) {
        const that = new WeakRef(this);
        
        setTimeout(function () {
            that.get()._allRuns = new ObservableArray([])
            that.get().loadAllRuns(false);
            const listView = args.object;
            listView.notifyPullToRefreshFinished();
        }, 1000);
    }

    dateDayNumber(value: string) {
        if (value) {
            return moment(String(value)).format('DD');
        }
    }

    dateMonthText(value: string) {
        if (value) {
            return moment(String(value)).format('MMMM');
        }
    }

    dateYearNumber(value: string) {
        if (value) {
            return moment(String(value)).format('Y');
        }
    }

    tabControl(val: string) {
        if (val == 'all') {
            this.tab1 = true;
            this.tab2 = false;
        } else if (val == 'userAttended') {
            this.tab1 = false;
            this.tab2 = true;
        }
    }

}
