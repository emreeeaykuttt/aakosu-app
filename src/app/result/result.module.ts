import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { NativeScriptUIListViewModule } from 'nativescript-ui-listview/angular';

import { ResultListComponent } from './result-list/result-list.component';

@NgModule({
    declarations: [
        ResultListComponent
    ],
    imports: [
        NativeScriptCommonModule,
        NativeScriptUIListViewModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forChild([
            { path: '', redirectTo: 'result-list', pathMatch: 'full' },
            { path: 'result-list', component: ResultListComponent },
        ]),
        TNSFontIconModule.forRoot({
			'la': require('~/assets/line-awesome.min.css'),
        }),
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class ResultModule { }
