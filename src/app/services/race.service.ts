import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError, retry } from 'rxjs/operators';

import { Race } from '../models/race.model';
import { API } from './api';

@Injectable()
export class RaceService {

    race: Race;

    constructor(private http: HttpClient) {
        this.race = new Race();
    }
    
    getRaces() {
        let user_id = localStorage.getItem('user_id');
        let params = new HttpParams()
                .set('user_id', user_id)
                .set('month_id', '0')
                .set('city_id', '0');

        return this.http.get(
            API.baseUrl + '/races', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    getRacesFilter(data: any) {
        let user_id = localStorage.getItem('user_id');
        let params = new HttpParams()
            .set('user_id', user_id)
            .set('month_id', data.monthId)
            .set('city_id', data.cityId);
            
        return this.http.get(
            API.baseUrl + '/races', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    getRaceResults() {
        let user_id = localStorage.getItem('user_id');
        let params = new HttpParams()
            .set('user_id', user_id);
            
        return this.http.get(
            API.baseUrl + '/races/results', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    getRace(id: number) {
        let user_id = localStorage.getItem('user_id');
        let params = new HttpParams()
            .set('id', String(id))  // '/races/' + id
            .set('admin_group_id', '1') // silinecek
            .set('user_id', user_id);
            
        return this.http.get(
            API.baseUrl + '/admins/race_id', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    // Admin (start)
    getAdminRaces() {
        let user_id = localStorage.getItem('user_id');
        let admin_group_id = localStorage.getItem('admin_group_id');
        let params = new HttpParams()
            .set('user_id', user_id)
            .set('admin_group_id', admin_group_id)
            .set('month_id', '0')
            .set('city_id', '0');

        return this.http.get(
            API.baseUrl + '/admins/race', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    getAdminRacesFilter(data: any) {
        let user_id = localStorage.getItem('user_id');
        let admin_group_id = localStorage.getItem('admin_group_id');
        let params = new HttpParams()
            .set('user_id', user_id)
            .set('admin_group_id', admin_group_id)
            .set('month_id', data.monthId)
            .set('city_id', data.cityId);
            
        return this.http.get(
            API.baseUrl + '/admins/race', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    getAdminRace(id: number) {
        let user_id = localStorage.getItem('user_id');
        let admin_group_id = localStorage.getItem('admin_group_id');
        let params = new HttpParams()
            .set('id', String(id))
            .set('admin_group_id', admin_group_id)
            .set('user_id', user_id);
            
        return this.http.get(
            API.baseUrl + '/admins/race_id', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    save(race: Race) {
        let user_id = localStorage.getItem('user_id');
        let admin_group_id = localStorage.getItem('admin_group_id');
        
        if (!race.id) {
            return this.http.post(
                API.baseUrl + '/admins/race_add', {
                    admin_group_id: admin_group_id,
                    user_id: user_id,
                    title: race.title,
                    trail_id: race.trail_id,
                    start_date: race.start_date,
                    race_type: race.race_type,
                    start_hour: race.start_hour,
                    end_hour: race.end_hour,
                    min_time: race.min_time,
                    description: race.description,
                }, {
                    headers: this.getCommonHeaders()
                }
            )
            .pipe(
                catchError(this.handleErrors)
            );
        } else {
            return this.http.post(
                API.baseUrl + '/admins/race_update', {
                    id: race.id,
                    admin_group_id: admin_group_id,
                    user_id: user_id,
                    title: race.title,
                    trail_id: race.trail_id,
                    start_date: race.start_date,
                    race_type: race.race_type,
                    start_hour: race.start_hour,
                    end_hour: race.end_hour,
                    min_time: race.min_time,
                    max_time: 120,
                    description: race.description,
                }, {
                    headers: this.getCommonHeaders()
                }
            )
            .pipe(
                catchError(this.handleErrors)
            );
        }
    }

    delete(id: number) {
        let admin_group_id = localStorage.getItem('admin_group_id');
        let params = new HttpParams()
            .set('race_id', String(id))
            .set('admin_group_id', admin_group_id);
            
        return this.http.delete(
            API.baseUrl + '/admins/race_remove', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }
    // Admin (end)

    private getCommonHeaders() {
        return new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + API.token,
        });
    }
    
    private handleErrors(error: HttpErrorResponse) {
        return throwError(error);
    }
}