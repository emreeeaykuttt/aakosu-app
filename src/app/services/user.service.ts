import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import * as localStorage from 'nativescript-localstorage';

import { User } from '../models/user.model';
import { API } from './api';
 
@Injectable()
export class UserService {

    user: User;
    
    constructor(private http: HttpClient) {
        this.user = new User();
    }

    getUser() {
        let user_id = localStorage.getItem('user_id');

        return this.http.get(
            API.baseUrl + '/users/' + user_id
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    getUsers(search_text: string) {
        let params = new HttpParams()
            .set('search_text', search_text);
            
        return this.http.get(
            API.baseUrl + '/users/user_list', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    login(user: User) {
        API.token = 'wCOeZvdTNKaIraLeiZCyzzyOyRRdzAjaesFmKIYJLzJFDaLxAC';
        localStorage.setItem('user_id', '289610');
        localStorage.setItem('admin_group_id', '1');
        // API.userId = '295247' 289617 289610 289623
        return true;

        // let device_token = localStorage.getItem('device_token');
        // return this.http.post(
        //     'http://192.168.1.105:1007/api/auth/login', 
        //     {
        //         device_token: device_token
        //         email: user.email,
        //         password: user.password,
        //     }
        // )
        // .pipe(
        //     tap((data: any) => {
        //         API.token = data.access_token
        //         API.userId = data.id
        //         API.adminGroupId = data.admin_group_id
        //         localStorage.setItem('access_token', data.access_token)
        //         localStorage.setItem('user_id', data.id)
        //         localStorage.setItem('admin_group_id', data.admin_group_id)
        //     }),
        //     catchError(this.handleErrors)
        // );
    }

    register(user: User) {
        console.log(user);
        return true;

        // let device_token = localStorage.getItem('device_token');
        // return this.http.post(
        //     'http://192.168.1.105:1007/api/auth/register', 
        //     JSON.stringify({
        //         device_token: device_token
        //         firstname: user.firstname,
        //         lastname: user.lastname,
        //         email: user.email,
        //         password: user.password,
        //         is_contract: user.is_contract,
        //     })
        // )
        // .pipe(catchError(this.handleErrors));
    }

    logout() {
        API.token = ''
        localStorage.removeItem('user_id')
        localStorage.removeItem('admin_group_id')
        return true;
        // return this.http.post(
        //     'http://192.168.1.105:1007/api/auth/logout',
        //     {
        //         headers: this.getCommonHeaders() 
        //     }
        // )
        // .pipe(
        //     tap((data: any) => {
        //         API.token = ''
        //         // API.userId = ''
        //         // API.adminGroupId = ''
        //         localStorage.removeItem('access_token')
        //         localStorage.removeItem('user_id')
        //         localStorage.removeItem('admin_group_id')
        //     }),
        //     catchError(this.handleErrors)
        // );
    }

    update(user: User) {
        console.log(user);
        return true;

        // let user_id = localStorage.getItem('user_id');
        // let params = new HttpParams()
        //     .set('user_id', user_id);
        // return this.http.post(
        //     API.baseUrl + '/profiles', 
        //     JSON.stringify({
        //         firstname: user.firstname,
        //         lastname: user.lastname,
        //         email: user.email,
        //     })
        // )
        // .pipe(catchError(this.handleErrors));
    }

    getCities() {    
        return this.http.get(
            API.baseUrl + '/cities', {
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    getDistricts(city_id: number) {
        return this.http.get(
            API.baseUrl + '/districts/' + city_id, {
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    getRaceUnregisteredUsers (race_id: number, search_text: string) {
        let user_id = localStorage.getItem('user_id');
        let params = new HttpParams()
            .set('user_id', user_id)
            .set('race_id', String(race_id))
            .set('search_text', search_text);
            
        return this.http.get(
            API.baseUrl + '/races/users', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    getRaceRegisteredUsers (race_id: number, search_text: string) {
        let params = new HttpParams()
            .set('search_text', search_text);
            
        return this.http.get(
            API.baseUrl + '/raceusers/users/' + race_id, {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }
    
    private getCommonHeaders() {
        return new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + API.token,
        });
    }
    
    private handleErrors(error: HttpErrorResponse) {
        return throwError(error);
    }

}