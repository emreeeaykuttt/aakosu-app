import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Notification } from '../models/notification.model';
import { API } from './api';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    notification: Notification;

    constructor(private http: HttpClient) {
        this.notification = new Notification();
    }
    
    getNotifications() {
        let user_id = localStorage.getItem('user_id');
        let params = new HttpParams().set('receiver_user_id', user_id);
        
        return this.http.get(
            API.baseUrl + '/notifications', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    getNotificationCount() {
        let user_id = localStorage.getItem('user_id');
        let params = new HttpParams().set('receiver_user_id', user_id);

        return this.http.get(
            API.baseUrl + '/notifications/unviewed_list', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    allViewed() {
        let user_id = localStorage.getItem('user_id');

        return this.http.post(
            API.baseUrl + '/notifications/all_viewed', {
                receiver_user_id: user_id
            },
            {headers: this.getCommonHeaders()}
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    readed(id: number) {
        return this.http.post(
            API.baseUrl + '/notifications/readed', {
                notification_id: id,
            }, {
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    acceptRaceInvitation(id: number, race_id: number) {
        let user_id = localStorage.getItem('user_id');

        return this.http.post(
            API.baseUrl + '/notifications/accept_race_invitation', {
                notification_id: id,
                race_id: race_id,
                user_id: user_id,
            }, {
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    rejectRaceInvitation(id: number) {
        return this.http.post(
            API.baseUrl + '/notifications/reject_race_invitation', {
                notification_id: id,
            }, {
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    acceptTeamInvitation(id: number, team_id: number) {
        let user_id = localStorage.getItem('user_id');

        return this.http.post(
            API.baseUrl + '/notifications/accept_team_invitation', {
                notification_id: id,
                team_id: team_id,
                user_id: user_id,
            }, {
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    rejectTeamInvitation(id: number, team_id: number) {
        let user_id = localStorage.getItem('user_id');

        return this.http.post(
            API.baseUrl + '/notifications/reject_team_invitation', {
                notification_id: id,
                team_id: team_id,
                user_id: user_id,
            }, {
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    acceptTeamRegisterRequest(id: number, team_id: number, sender_user_id: number) {
        let user_id = localStorage.getItem('user_id');

        return this.http.post(
            API.baseUrl + '/notifications/accept_team_register_request', {
                notification_id: id,
                team_id: team_id,
                sender_user_id: sender_user_id,
                user_id: user_id,
            }, {
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    rejectTeamRegisterRequest(id: number, team_id: number, sender_user_id: number) {
        let user_id = localStorage.getItem('user_id');

        return this.http.post(
            API.baseUrl + '/notifications/reject_team_register_request', {
                notification_id: id,
                team_id: team_id,
                sender_user_id: sender_user_id,
                user_id: user_id,
            }, {
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    saveNotification(notification: Notification, user_ids: string, user_device_tokens: string) {
        // return this.http.post(
        //     API.baseUrl + '/admins/notifications/add', {
        //         user_ids: user_ids,
        //         user_device_tokens: user_device_tokens,
        //         title: notification.title,
        //         description: notification.description,
        //     }, {
        //         headers: this.getCommonHeaders()
        //     }
        // )
        // .pipe(
        //     catchError(this.handleErrors)
        // );

        let user_id = localStorage.getItem('user_id');

        return this.http.post(
            API.baseUrl + '/notifications/all_viewed', {
                receiver_user_id: user_id
            },
            {headers: this.getCommonHeaders()}
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    sendNotification(device_token: string, title: string, description: string) {
        return this.http.post(
            'https://fcm.googleapis.com/fcm/send', {
                notification: {
                    title: title,
                    text: description,
                    sound: 'default',
                },
                priority: 'High',
                to: device_token
            }, {
                headers: this.getFirebaseHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    private getFirebaseHeaders() {
        return new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'key=AAAA4uau7OM:APA91bFd1b4D-d1nB-lEpjWm4hQZ4aFc1SHCgmyU0nM4u99BbXGwipssJrkeLiouEFjCPusiaEgknmYlNJt9Rf6MU378GpMcyRiafairLV2tb4d2pSyiZ0HWxZ8WlYvfrIwiabRSmlP_',
        });
    }

    private getCommonHeaders() {
        return new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + API.token,
        });
    }
    
    private handleErrors(error: HttpErrorResponse) {
        return throwError(error);
    }
}