import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError, retry } from 'rxjs/operators';
import * as moment from 'moment';

import { RaceUser } from '../models/race-user.model';
import { API } from './api';

@Injectable({
    providedIn: 'root'
})
export class RaceUserService {

    race: RaceUser;

    constructor(private http: HttpClient) {
        this.race = new RaceUser();
    }

    getRaceUser(race_id: number) {
        let user_id = localStorage.getItem('user_id');
        let params = new HttpParams()
            .set('user_id', user_id)
            .set('race_id', String(race_id));
            
        return this.http.get(
            API.baseUrl + '/raceusers/user', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    getUserAttendedRuns() {
        let user_id = localStorage.getItem('user_id');
        let params = new HttpParams()
            .set('user_id', user_id);
            
        return this.http.get(
            API.baseUrl + '/raceusers', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    getUserBestRun() {
        let user_id = localStorage.getItem('user_id');
        let params = new HttpParams()
            .set('user_id', user_id);
            
        return this.http.get(
            API.baseUrl + '/raceusers/best_running', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }
    
    raceUserRegister(race_id: number) {
        let user_id = localStorage.getItem('user_id');
        let nowDateTime = moment(String(new Date())).format('Y-MM-DD HH:mm:ss');

        return this.http.post(
            API.baseUrl + '/raceusers/register', {
                race_id: race_id,
                user_id: user_id,
                created_at: nowDateTime
            }, {
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    raceUserStart(race_id: number) {
        let user_id = localStorage.getItem('user_id');

        return this.http.post(
            API.baseUrl + '/raceusers/start', {
                race_id: race_id,
                user_id: user_id,
            }, {
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    raceUserAddCalendar(race_id: number) {
        let user_id = localStorage.getItem('user_id');

        return this.http.post(
            API.baseUrl + '/raceusers/add_calendar', {
                race_id: race_id,
                user_id: user_id,
            }, {
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    raceUserFriendInvite(race_id: number, race_users: any) {
        let user_id = localStorage.getItem('user_id');
        
        return this.http.post(
            API.baseUrl + '/raceusers/create', {
                user_id: user_id,
                race_id: race_id,
                race_users: race_users,
            }, {
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    // Admin (start)
    getAdminRegisteredRaceUsers(race_id: number, gender_val: string) {
        // let filter_type = (gender_val != '' ?  'gender' : ''); api güncellenince bu satır aktifleşecek
        let filter_type = (gender_val != '' ?  'gender' : 'gender');
        let params = new HttpParams()
            .set('filter_type', filter_type)
            .set('filter_val', gender_val)
            .set('race_id', String(race_id));
            
        return this.http.get(
            API.baseUrl + '/admins/race_user_list', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    getAdminRegisteredTeams(race_id: number) {
        let params = new HttpParams()
            .set('race_id', String(race_id));
            
        return this.http.get(
            API.baseUrl + '/admins/team_list', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }
    // Admin (end)

    private getCommonHeaders() {
        return new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + API.token,
        });
    }
    
    private handleErrors(error: HttpErrorResponse) {
        return throwError(error);
    }
}