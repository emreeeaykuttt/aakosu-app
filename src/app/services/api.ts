import { Injectable } from '@angular/core';
import { getString, setString } from 'tns-core-modules/application-settings';

const tokenKey = '';

@Injectable()
export class API {

    static _notificationCount: string = '';
    
    static baseUrl = 'https://adimadim-staging.herokuapp.com/api/v2';

    static isLoggedIn(): boolean {
        return !!getString(tokenKey);
    }
  
    static get token(): string {
        return getString(tokenKey);
    }
  
    static set token(theToken: string) {
        setString(tokenKey, theToken);
    }

    static get notificationCount(): string {
        return this._notificationCount;
    }
  
    static set notificationCount(theNotificationCount: string) {
        this._notificationCount = theNotificationCount;
    }

}