import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError, retry } from 'rxjs/operators';

import { Trail } from '../models/trail.model';
import { API } from './api';

@Injectable({
    providedIn: 'root'
})
export class TrailService {

    trail: Trail;

    constructor(private http: HttpClient) {
        this.trail = new Trail();
    }
    
    getAdminTrails() {
        let admin_group_id = localStorage.getItem('admin_group_id');
        let params = new HttpParams()
                .set('admin_group_id', admin_group_id);

        return this.http.get(
            API.baseUrl + '/admins/trail', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    getAdminTrail(id: number) {
        let admin_group_id = localStorage.getItem('admin_group_id');
        let params = new HttpParams()
            .set('id', String(id))
            .set('admin_group_id', admin_group_id);
            
        return this.http.get(
            API.baseUrl + '/admins/trail_id', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    save(trail: Trail) {
        let user_id = localStorage.getItem('user_id');
        let admin_group_id = localStorage.getItem('admin_group_id');
        
        if (!trail.id) {
            return this.http.post(
                API.baseUrl + '/admins/trail_add', {
                    admin_group_id: admin_group_id,
                    user_id: user_id,
                    name: trail.track_name,
                    racecourse: trail.racecourse,
                    city_id: trail.city_id,
                    start_latitude: trail.start_latitude,
                    start_longitude: trail.start_longitude,
                    end_latitude: trail.end_latitude,
                    end_longitude: trail.end_longitude,
                    open_address: trail.open_address,
                }, {
                    headers: this.getCommonHeaders()
                }
            )
            .pipe(
                catchError(this.handleErrors)
            );
        } else {
            return this.http.post(
                API.baseUrl + '/admins/trail_update', {
                    id: trail.id,
                    admin_group_id: admin_group_id,
                    user_id: user_id,
                    name: trail.track_name,
                    racecourse: trail.racecourse,
                    city_id: trail.city_id,
                    start_latitude: trail.start_latitude,
                    start_longitude: trail.start_longitude,
                    end_latitude: trail.end_latitude,
                    end_longitude: trail.end_longitude,
                    open_address: trail.open_address,
                }, {
                    headers: this.getCommonHeaders()
                }
            )
            .pipe(
                catchError(this.handleErrors)
            );
        }
    }

    delete(id: number) {
        let admin_group_id = localStorage.getItem('admin_group_id');
        let params = new HttpParams()
            .set('trail_id', String(id))
            .set('admin_group_id', admin_group_id);
            
        return this.http.delete(
            API.baseUrl + '/admins/trail_remove', {
                params,
                headers: this.getCommonHeaders()
            }
        )
        .pipe(
            catchError(this.handleErrors)
        );
    }

    private getCommonHeaders() {
        return new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + API.token,
        });
    }
    
    private handleErrors(error: HttpErrorResponse) {
        return throwError(error);
    }
}