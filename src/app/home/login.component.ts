import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RouterExtensions } from "nativescript-angular/router";
import * as dialogs from 'tns-core-modules/ui/dialogs';

import { UserService } from '../services/user.service';
import { User } from '../models/user.model';

@Component({
    selector: 'Login',
    templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {

    loading = false;
    user: User;
    @ViewChild('password', { static: false }) password: ElementRef;

    constructor(private routerExtensions: RouterExtensions, private userService: UserService) { 
        this.user = new User();
    }

    onNavBtnTap(){
        this.routerExtensions.navigate(['/home'], {clearHistory: true});
    }

    ngOnInit(): void {
    }

    login(args) {

        let validator = require('email-validator');
        let password = this.password.nativeElement
        this.loading = true;

        if (!validator.validate(this.user.email)) {
            alert({
                title: 'Uyarı',
                message: 'Lütfen geçerli bir E-Posta adresi giriniz.',
                okButtonText: 'Tamam',
                cancelable: false
            });

            this.loading = false;
            return;
        }
        else
        {
            this.userService.login(this.user)
            this.loading = false;
            this.routerExtensions.navigate(['/tabs/default'], {clearHistory: true});
                // .subscribe(
                //     (res) => {
                //         this.loading = false;
                //         this.routerExtensions.navigate(['/tabs/default'], {clearHistory: true});
                //     },
                //     (error) => {
                //         dialogs.alert({
                //             title: 'Uyarı',
                //             message: error.error.message,
                //             okButtonText: 'Tamam',
                //             cancelable: false
                //         });

                //         password.text = '';
                //         this.loading = false;
                //     }
                // );
        }
    }

}
