import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RouterExtensions } from "nativescript-angular/router";
import * as dialogs from 'tns-core-modules/ui/dialogs';

import { UserService } from '../services/user.service';
import { User } from '../models/user.model';

@Component({
    selector: 'Register',
    templateUrl: './register.component.html',
})
export class RegisterComponent implements OnInit {

    loading = false;
    user: User;
    @ViewChild('email', { static: false }) email: ElementRef;
    @ViewChild('firstname', { static: false }) firstname: ElementRef;
    @ViewChild('lastname', { static: false }) lastname: ElementRef;
    @ViewChild('password', { static: false }) password: ElementRef;
    @ViewChild('passwordAgain', { static: false }) passwordAgain: ElementRef;
    @ViewChild('isContract', { static: false }) isContract: ElementRef;
    @ViewChild('errorEmail', { static: false }) errorEmail: ElementRef;
    @ViewChild('errorFirstname', { static: false }) errorFirstname: ElementRef;
    @ViewChild('errorLastname', { static: false }) errorLastname: ElementRef;
    @ViewChild('errorPassword', { static: false }) errorPassword: ElementRef;
    @ViewChild('errorPasswordAgain', { static: false }) errorPasswordAgain: ElementRef;
    @ViewChild('errorIsContract', { static: false }) errorIsContract: ElementRef;
  
    constructor(private routerExtensions: RouterExtensions, private userService: UserService) { 
        this.user = new User();
    }

    onNavBtnTap(){
        this.routerExtensions.navigate(['/home'], {clearHistory: true});
    }

    ngOnInit(): void {

    }

    register(args) {

        let validate = this.validateRegister();

        if(validate)
        {
            this.loading = true;

            this.userService.register(this.user)
            this.loading = false;
            dialogs.alert({
                title: 'Başarılı',
                message: 'Üyelik başarılı bir şekilde gerçekleşti. Şimdi giriş yapabilirsiniz.',
                okButtonText: 'Tamam',
                cancelable: false 
            }).then(() => {
                this.email.nativeElement.text = '';
                this.firstname.nativeElement.text = '';
                this.lastname.nativeElement.text = '';
                this.password.nativeElement.text = '';
                this.passwordAgain.nativeElement.text = '';
                this.isContract.nativeElement.toggle();
                this.routerExtensions.navigate(['/login'], {clearHistory: true});
            });
                // .subscribe(
                //     (res) => {
                //         dialogs.alert({
                //             title: 'Başarılı',
                //             message: res['message'],
                //             okButtonText: "Tamam",
                //             cancelable: false 
                //         }).then(() => {
                //             this.email.nativeElement.text = '';
                //             this.firstname.nativeElement.text = '';
                //             this.lastname.nativeElement.text = '';
                //             this.password.nativeElement.text = '';
                //             this.passwordAgain.nativeElement.text = '';
                //             this.isContract.nativeElement.toggle();
                //             this.routerExtensions.navigate(['/login']);
                //         });

                //         this.loading = false;
                //     },
                //     (error) => {
                //         dialogs.alert({
                //             title: 'Uyarı',
                //             message: error.error.message,
                //             okButtonText: 'Tamam',
                //             cancelable: false
                //         });

                //         this.loading = false;
                //     }
                // );
        }
    }

    validateRegister()
    {
        let status = true;
        let validator = require('email-validator');
        let errorMessage = {
            email: this.errorEmail.nativeElement,
            firstname: this.errorFirstname.nativeElement,
            lastname: this.errorLastname.nativeElement,
            password: this.errorPassword.nativeElement,
            passwordAgain: this.errorPasswordAgain.nativeElement,
            isContract: this.errorIsContract.nativeElement,
        }
        
        if(!this.user.email)
        {
            errorMessage.email.visibility = 'visible';
            errorMessage.email.text = '*E-Posta alanı boş bırakılamaz';
            status = false;
        }
        else if(!validator.validate(this.user.email))
        {
            errorMessage.email.visibility = 'visible';
            errorMessage.email.text = '*Geçerli bir E-Posta adresi giriniz.';
            status = false;
        }
        else
        {
            errorMessage.email.visibility = 'collapse';
            errorMessage.email.text = '';
        }

        if(!this.user.firstname)
        {
            errorMessage.firstname.visibility = 'visible';
            errorMessage.firstname.text = '*İsim alanı boş bırakılamaz';
            status = false;
        }
        else
        {
            errorMessage.firstname.visibility = 'collapse';
            errorMessage.firstname.text = '';
        }

        if(!this.user.lastname)
        {
            errorMessage.lastname.visibility = 'visible';
            errorMessage.lastname.text = '*Soyisim alanı boş bırakılamaz';
            status = false;
        }
        else
        {
            errorMessage.lastname.visibility = 'collapse';
            errorMessage.lastname.text = '';
        }

        if(!this.user.password)
        {
            errorMessage.password.visibility = 'visible';
            errorMessage.password.text = '*Şifre alanı boş bırakılamaz';
            status = false;
        }
        else
        {
            errorMessage.password.visibility = 'collapse';
            errorMessage.password.text = '';
        }

        if(this.user.password != this.user.password_again)
        {
            errorMessage.passwordAgain.visibility = 'visible';
            errorMessage.passwordAgain.text = '*Şifre Tekrar alanı Şifre alanı ile eşleşmiyor';
            status = false;
        }
        else
        {
            errorMessage.passwordAgain.visibility = 'collapse';
            errorMessage.passwordAgain.text = '';
        }

        if(!this.user.is_contract)
        {
            errorMessage.isContract.visibility = 'visible';
            errorMessage.isContract.text = '*Sözleşmeyi onaylayınız';
            status = false;
        }
        else
        {
            errorMessage.isContract.visibility = 'collapse';
            errorMessage.isContract.text = '';
        }

        return status;
    }
}
