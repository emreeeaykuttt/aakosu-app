import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { NativeScriptUIListViewModule } from 'nativescript-ui-listview/angular';

import { NotificationListComponent } from './notification-list/notification-list.component';
import { NotificationInviteUserToTeamAcceptAndRejectComponent } from './notification-invite-user-to-team-accept-and-reject/notification-invite-user-to-team-accept-and-reject.component';
import { NotificationInviteUserToRaceComponent } from './notification-invite-user-to-race/notification-invite-user-to-race.component';
import { NotificationInviteUserToTeamComponent } from './notification-invite-user-to-team/notification-invite-user-to-team.component';
import { NotificationTeamRegisterRequestComponent } from './notification-team-register-request/notification-team-register-request.component';
import { NotificationTeamRegisterRequestAcceptAndRejectComponent } from './notification-team-register-request-accept-and-reject/notification-team-register-request-accept-and-reject.component';

@NgModule({
    declarations: [
        NotificationListComponent,
        NotificationInviteUserToTeamAcceptAndRejectComponent,
        NotificationInviteUserToRaceComponent,
        NotificationInviteUserToTeamComponent,
        NotificationTeamRegisterRequestComponent,
        NotificationTeamRegisterRequestAcceptAndRejectComponent
    ],
    imports: [
        NativeScriptCommonModule,
        NativeScriptUIListViewModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forChild([
            { path: '', redirectTo: 'notification-list', pathMatch: 'full' },
            { path: 'notification-list', component: NotificationListComponent },
            { path: 'notification-invite-user-to-race/:id/:raceId/:teamId/:senderUserId/:title', component: NotificationInviteUserToRaceComponent },
            { path: 'notification-invite-user-to-team/:id/:raceId/:teamId/:senderUserId/:title', component: NotificationInviteUserToTeamComponent },
            { path: 'notification-invite-user-to-team-accept-and-reject/:id/:raceId/:teamId/:senderUserId/:title', component: NotificationInviteUserToTeamAcceptAndRejectComponent },
            { path: 'notification-team-register-request/:id/:raceId/:teamId/:senderUserId/:title', component: NotificationTeamRegisterRequestComponent },
            { path: 'notification-team-register-request-accept-and-reject/:id/:raceId/:teamId/:senderUserId/:title', component: NotificationTeamRegisterRequestAcceptAndRejectComponent },
        ]),
        TNSFontIconModule.forRoot({
			'la': require('~/assets/line-awesome.min.css'),
        }),
    ],
    providers: [
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class NotificationModule { }
