import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import * as dialogs from 'tns-core-modules/ui/dialogs';

import { NotificationService } from '../../services/notification.service';
import { RaceService } from '../../services/race.service';
import { Race } from '../../models/race.model';

@Component({
    selector: 'NotificationTeamRegisterRequest',
    moduleId: module.id,
    templateUrl: './notification-team-register-request.component.html',
    styleUrls: ['./notification-team-register-request.component.css']
})
export class NotificationTeamRegisterRequestComponent implements OnInit {
    
    loading = false;
    notificationId: number;
    raceId: number;
    teamId: number;
    senderUserId: number;
    notificationTitle: string;
    race: Race;
    subscription: Subscription;

    constructor(
        private notificationService: NotificationService,
        private raceService: RaceService,
        private route: ActivatedRoute,
        private routerExtension: RouterExtensions,
    ) { 
        let trLocale = require('moment/locale/tr');
        moment.locale('tr', trLocale);
    }

    ngOnInit(): void {
        this.race = new Race();
        this.subscription = this.route.params.subscribe(params => {
            this.notificationId = params['id'];
            this.notificationTitle = params['title'];
            this.raceId = params['raceId'];
            this.teamId = params['teamId'];
            this.senderUserId = params['senderUserId'];
            this.loadRace(this.raceId);
        });
    }

    loadRace(race_id: number) {
        this.loading = true;
        this.raceService.getRace(race_id)
            .subscribe((result) => {
                this.race = result[0];
                this.loading = false;
            }, (error) => {
                console.log(error);
            });
    }

    accept() {
        this.notificationService.acceptTeamRegisterRequest(this.notificationId, this.teamId, this.senderUserId)
            .subscribe((result) => {
                if (result['status']) {
                    dialogs.alert({
                        title: 'Başarılı',
                        message: result['message'],
                        okButtonText: 'Tamam',
                        cancelable: false 
                    }).then(() => {
                        this.goNotificationList();
                    });
                } else {
                    dialogs.alert({
                        title: 'Uyarı',
                        message: result['message'],
                        okButtonText: 'Tamam',
                        cancelable: false 
                    });
                }
            });
    }

    reject() {
        this.notificationService.rejectTeamRegisterRequest(this.notificationId, this.teamId, this.senderUserId)
            .subscribe((result) => {
                if (result['status']) {
                    dialogs.alert({
                        title: 'Başarılı',
                        message: result['message'],
                        okButtonText: 'Tamam',
                        cancelable: false 
                    }).then(() => {
                        this.goNotificationList();
                    });
                } else {
                    dialogs.alert({
                        title: 'Uyarı',
                        message: result['message'],
                        okButtonText: 'Tamam',
                        cancelable: false 
                    });
                }
            });
    }

    goNotificationList() {
        this.routerExtension.navigate(['notification-list'], { relativeTo: this.route.parent });
    }

    dayAndMonthConvert(value: string) {
        if (value) {
            return moment(String(value)).format('DD MMMM')
        }
    }

    yearConvert(value: string) {                
        if (value) {
            return moment(String(value)).format('Y')
        }
    }

    timeConvert(value: string) {
        if (value) {
            return moment(String(value)).format('HH:mm')
        }
    }

}
