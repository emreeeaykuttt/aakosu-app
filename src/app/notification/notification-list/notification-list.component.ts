import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { ObservableArray } from 'tns-core-modules/data/observable-array';
import { RadListViewComponent } from 'nativescript-ui-listview/angular';
import { ListViewEventData } from 'nativescript-ui-listview';
import * as moment from 'moment';

import { API } from '../../services/api';
import { NotificationService } from '../../services/notification.service';
import { Notification } from '../../models/notification.model';

@Component({
    selector: 'NotificationList',
    templateUrl: './notification-list.component.html',
    styleUrls: ['./notification-list.component.css']
})
export class NotificationListComponent implements OnInit {

    loading = false;
    _notificationItems: ObservableArray<Notification> = new ObservableArray([]); 
    @ViewChild('listView', { static: false }) listView: RadListViewComponent;

    constructor(
        private route: ActivatedRoute, 
        private routerExtension: RouterExtensions,
        private notificationService: NotificationService
    ) {
        let trLocale = require('moment/locale/tr');
        moment.locale('tr', trLocale);
    }

    public get notificationItems(): ObservableArray<Notification> {
        return this._notificationItems;
    }

    ngOnInit(): void {
        this.loadNotificationList();
    }

    loadNotificationList(runLoading: boolean = true) {
        this.loading = runLoading;
        this.notificationService.getNotifications()
            .subscribe((result) => {
                this._notificationItems.push(result);
                this.loading = false;
            }, (error) => {
                console.log(error);
            });
    }

    onPullToRefreshInitiated(args: ListViewEventData) {
        const that = new WeakRef(this);
        
        setTimeout(function () {
            that.get()._notificationItems = new ObservableArray([])
            that.get().loadNotificationList(false);
            API.notificationCount = '';
            const listView = args.object;
            listView.notifyPullToRefreshFinished();
        }, 1000);
    }

    differenceFind(notificationDate) {
        const createdAt = moment(String(notificationDate)).format('YYYY/MM/DD HH:mm')
        var diff = Math.abs(<any>new Date() - <any>new Date(createdAt));
        var minutes = Math.floor((diff/1000)/60);
        var hour = Math.floor(minutes / 60);
        var day = Math.floor(hour / 24);
        
        if(minutes < 60) {
            return minutes + ' dakika'
        }
        else if(hour < 24) {
            return hour + ' saat'
        }
        else {
            return day + ' gün'
        }
    }

    notificationDetail(type: string, notificationId: number, raceId: number, teamId: number, senderUserId: number, title: string, opened: number) {
        if(opened == 0)
        {
            this.notificationService.readed(notificationId);
        }

        if (type == 'notificationInviteUserToRace') {
            this.routerExtension.navigate(['notification-invite-user-to-race/' + notificationId + '/' + raceId + '/' + teamId + '/' + senderUserId + '/' + title], { relativeTo: this.route.parent });
        } else if (type == 'notificationInviteUserToTeam') {
            this.routerExtension.navigate(['notification-invite-user-to-team/' + notificationId + '/' + raceId + '/' + teamId + '/' + senderUserId + '/' + title], { relativeTo: this.route.parent });
        } else if (type == 'notificationInviteUserToTeamAcceptAndReject') {
            this.routerExtension.navigate(['notification-invite-user-to-team-accept-and-reject/' + notificationId + '/' + raceId + '/' + teamId + '/' + senderUserId + '/' + title], { relativeTo: this.route.parent });
        } else if (type == 'notificationTeamRegisterRequest') {
            this.routerExtension.navigate(['notification-team-register-request/' + notificationId + '/' + raceId + '/' + teamId + '/' + senderUserId + '/' + title], { relativeTo: this.route.parent });
        } else if (type == 'notificationTeamRegisterRequestAcceptAndReject') {
            this.routerExtension.navigate(['notification-team-register-request-accept-and-reject/' + notificationId + '/' + raceId + '/' + teamId + '/' + senderUserId + '/' + title], { relativeTo: this.route.parent });
        }
    }

}
