import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { Subscription } from 'rxjs';

@Component({
    selector: 'NotificationInviteUserToTeamAcceptAndReject',
    moduleId: module.id,
    templateUrl: './notification-invite-user-to-team-accept-and-reject.component.html',
    styleUrls: ['./notification-invite-user-to-team-accept-and-reject.component.css']
})
export class NotificationInviteUserToTeamAcceptAndRejectComponent implements OnInit {
    
    loading = false;
    notificationId: number;
    raceId: number;
    teamId: number;
    senderUserId: number;
    notificationTitle: string;
    subscription: Subscription;

    constructor(
        private route: ActivatedRoute,
        private routerExtension: RouterExtensions,
    ) { 
    }

    ngOnInit(): void {
        this.subscription = this.route.params.subscribe(params => {
            this.notificationId = params['id'];
            this.notificationTitle = params['title'];
            this.raceId = params['raceId'];
            this.teamId = params['teamId'];
            this.senderUserId = params['senderUserId'];
        });
    }

    goNotificationList() {
        this.routerExtension.navigate(['notification-list'], { relativeTo: this.route.parent });
    }

}
