import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NativeScriptRouterModule } from 'nativescript-angular/router';

import { CampaignMainComponent } from './campaign-main/campaign-main.component';

@NgModule({
    declarations: [
        CampaignMainComponent
    ],
    imports: [
        NativeScriptCommonModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forChild([
            { path: '', redirectTo: 'campaign-main', pathMatch: 'full' },
            { path: 'campaign-main', component: CampaignMainComponent },
        ]),
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class CampaignModule { }
