import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptRouterModule, NSEmptyOutletComponent } from 'nativescript-angular/router';
import { NativeScriptCommonModule } from 'nativescript-angular/common';

import { TabsComponent } from './tabs.component';

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forChild([
            // { path: '', redirectTo: 'default/(profileTab:profile-tab//raceTab:race-list)', pathMatch: 'full' },
            {
                path: 'default', component: TabsComponent, children: [
                    {
                        path: 'campaign-main',
                        outlet: 'campaignTab',
                        component: NSEmptyOutletComponent,
                        loadChildren: () => import('~/app/campaign/campaign.module').then(m => m.CampaignModule),
                    },
                    {
                        path: 'result-list',
                        outlet: 'resultTab',
                        component: NSEmptyOutletComponent,
                        loadChildren: () => import('~/app/result/result.module').then(m => m.ResultModule),
                    },
                    {
                        path: 'race-list',
                        outlet: 'raceTab',
                        component: NSEmptyOutletComponent,
                        loadChildren: () => import('~/app/race/race.module').then(m => m.RaceModule),
                    },
                    {
                        path: 'notification-list',
                        outlet: 'notificationTab',
                        component: NSEmptyOutletComponent,
                        loadChildren: () => import('~/app/notification/notification.module').then(m => m.NotificationModule),
                    },
                    {
                        path: 'profile-main',
                        outlet: 'profileTab',
                        component: NSEmptyOutletComponent,
                        loadChildren: () => import('~/app/profile/profile.module').then(m => m.ProfileModule),
                    },
                ]
            }
        ])
    ],
    declarations: [
        TabsComponent
    ],
    providers: [
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class TabsModule { }
