import { Component } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { ActivatedRoute } from '@angular/router';
import * as platform from 'tns-core-modules/platform';
import * as firebase from 'nativescript-plugin-firebase';
import * as localStorage from 'nativescript-localstorage';
import { Page } from "tns-core-modules/ui/page";

import { API } from '../services/api';
import { NotificationService } from '../services/notification.service';

@Component({
    moduleId: module.id,
    selector: 'TabsPage',
    templateUrl: './tabs.component.html',
    styleUrls: ['./tabs.component.css']
})
export class TabsComponent {

    constructor(
        private notificationService: NotificationService,
        private routerExtension: RouterExtensions,
        private hideActionbar:  Page,
        private activeRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.routerExtension.navigate([{ outlets: { campaignTab: ['campaign-main'], resultTab: ['result-list'], raceTab: ['race-list'], notificationTab: ['notification-list'], profileTab: ['profile-main'] } }], { relativeTo: this.activeRoute });
        this.loadNotificationCount();
        this.hideActionbar.actionBarHidden = true;

    }

    loadNotificationCount() {
        this.notificationService.getNotificationCount()
            .subscribe((result) => {
                if (result > 0) {
                    API.notificationCount = String(result);   
                }
            }, (error) => {
                console.log(error)
            });
    }

    notificationCount() {
        return API.notificationCount;
    }

    allViewed() {
        this.notificationService.allViewed()
            .subscribe((result) => {
                console.log(result['data']);
            });
    }

    campaingRefresh(args: any): void {
        console.log('campaing-refresh.......');
    }

    resultRefresh(args: any): void {
        console.log('result-refresh.......');
    }

    raceRefresh(args: any): void {
        console.log('race-refresh.......');
    }

    notificationRefresh(args: any): void {
        if (API.notificationCount != '') {
            this.allViewed();
        }

        API.notificationCount = '';
        console.log('notification-refresh.......');
    }

    profileRefresh(args: any): void {
        console.log('profile-refresh.......');
    }

    onBottomNavigationLoaded(args: any) {
        // const bottomNavigation = args.object;
        // const tabIndex = 1;
        // const badgeValue = "2";
        // if (platform.isIOS) {
        //   const tab = bottomNavigation.items[tabIndex];
        //   tab.badgeValue(badgeValue);
        // } else {
        //   bottomNavigation.setNotification(badgeValue, tabIndex);
        // }
    }
}
