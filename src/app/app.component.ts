import { Component, OnInit } from '@angular/core';
import * as firebase from 'nativescript-plugin-firebase';
import * as localStorage from 'nativescript-localstorage';

import { API } from './services/api';
import { NotificationService } from './services/notification.service';

@Component({
    selector: 'ns-app',
    moduleId: module.id,
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

    constructor(private notificationService: NotificationService) {
    }

    ngOnInit(): void {
        firebase.init({
            showNotifications: true,
            showNotificationsWhenInForeground: true,
            iOSEmulatorFlush: true,
    
            onPushTokenReceivedCallback: (token) => {
                localStorage.setItem('device_token', token);
                console.log('[Firebase] onPushTokenReceivedCallback:', { token });
            },
    
            onMessageReceivedCallback: (message: firebase.Message) => {
                    this.loadNotificationCount();
                    console.log('[Firebase] onMessageReceivedCallback:', { message });
                }
            })
            .then(() => {
                console.log('[Firebase] Initialized');
            })
            .catch(error => {
                console.log('[Firebase] Initialize', { error });
            });
    
    }

    loadNotificationCount() {
        this.notificationService.getNotificationCount()
            .subscribe((result) => {
                if (result > 0) {
                    API.notificationCount = String(result);
                }
            }, (error) => {
                console.log(error)
            });
    }

}


