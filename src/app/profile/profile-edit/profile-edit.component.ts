import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from "@angular/router";
import { SelectedIndexChangedEventData, ValueList } from 'nativescript-drop-down';
import * as dialogs from 'tns-core-modules/ui/dialogs';
import * as moment from 'moment';

import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';

@Component({
    selector: 'ProfileEdit',
    templateUrl: './profile-edit.component.html',
    styleUrls: ['./profile-edit.component.css']
})
export class ProfileEditComponent implements OnInit {

    loading = false;
    selectedGender = 0;
    selectedCountry = 0;
    selectedCity = 0;
    selectedDistrict = 0;
    user: User;
    genders: ValueList<string>;
    countries: ValueList<string>;
    cities: ValueList<string>;
    districts: ValueList<string>;
    @ViewChild('errorNationalID', { static: false }) errorNationalID: ElementRef;
    @ViewChild('errorEmail', { static: false }) errorEmail: ElementRef;
    @ViewChild('errorFirstname', { static: false }) errorFirstname: ElementRef;
    @ViewChild('errorLastname', { static: false }) errorLastname: ElementRef;
    @ViewChild('errorGender', { static: false }) errorGender: ElementRef;
    @ViewChild('errorBirthDate', { static: false }) errorBirthDate: ElementRef;
    @ViewChild('errorCountryID', { static: false }) errorCountryID: ElementRef;
    @ViewChild('errorCityID', { static: false }) errorCityID: ElementRef;
    @ViewChild('errorDistrictID', { static: false }) errorDistrictID: ElementRef;
    @ViewChild('errorTelephone', { static: false }) errorTelephone: ElementRef;
    @ViewChild('errorOrganisation', { static: false }) errorOrganisation: ElementRef;
    @ViewChild('errorTitle', { static: false }) errorTitle: ElementRef;

    constructor(private userService: UserService, private routerExtensions: RouterExtensions, private activeRoute: ActivatedRoute) {
        let trLocale = require('moment/locale/tr');
        moment.locale('tr', trLocale);
        this.user = new User();
    }

    ngOnInit(): void {
        this.genders = new ValueList<string>([
            {value: '0', display: 'Seçiniz...'},
            {value: 'Erkek', display: 'Erkek'},
            {value: 'Kadın', display: 'Kadın'},
            {value: 'Diğer', display: 'Diğer'},
        ]);

        this.countries = new ValueList<string>([
            {value: '0', display: 'Seçiniz...'},
            {value: 'TR', display: 'Türkiye'},
        ]);
        
        this.loadUser();
    }

    onNavBtnTap(){
        this.routerExtensions.navigate(['profile-main'], { relativeTo: this.activeRoute.parent });
    }

    updateUser() {

        let validate = this.validateUser();

        if(validate) {
            // this.loading = true;
            this.userService.update(this.user)
            dialogs.alert({
                title: 'Başarılı',
                message: 'Profil başarı bir şekilde güncellendi',
                okButtonText: 'Tamam',
                cancelable: false 
            }).then(() => {
                // this.routerExtensions.navigate(['/profile-main']);
                this.routerExtensions.navigate(['profile-main'], { relativeTo: this.activeRoute.parent });
            });
        } else{
            dialogs.alert({
                title: 'Uyarı',
                message: 'Gerekli alanları doldurunuz!',
                okButtonText: 'Tamam',
                cancelable: false 
            })
        }
        
    }

    validateUser() {

        let status = true;
        let validator = require('email-validator');
        let errorMessage = {
            nationalID: this.errorNationalID.nativeElement,
            email: this.errorEmail.nativeElement,
            firstname: this.errorFirstname.nativeElement,
            lastname: this.errorLastname.nativeElement,
            gender: this.errorGender.nativeElement,
            birthDate: this.errorBirthDate.nativeElement,
            countryID: this.errorCountryID.nativeElement,
            cityID: this.errorCityID.nativeElement,
            districtID: this.errorDistrictID.nativeElement,
            telephone: this.errorTelephone.nativeElement,
            organisation: this.errorOrganisation.nativeElement,
            title: this.errorTitle.nativeElement,
        }

        if(!this.user.national_id.trim()) {
            errorMessage.nationalID.visibility = 'visible';
            errorMessage.nationalID.text = '*TC Kimlik No alanı boş bırakılamaz';
            status = false;
        } else {
            errorMessage.nationalID.visibility = 'collapse';
            errorMessage.nationalID.text = '';
        }

        if(!this.user.email) {
            errorMessage.email.visibility = 'visible';
            errorMessage.email.text = '*E-Posta alanı boş bırakılamaz';
            status = false;
        } else if(!validator.validate(this.user.email)) {
            errorMessage.email.visibility = 'visible';
            errorMessage.email.text = '*Geçerli bir E-Posta adresi giriniz.';
            status = false;
        } else {
            errorMessage.email.visibility = 'collapse';
            errorMessage.email.text = '';
        }

        if(!this.user.firstname.trim()) {
            errorMessage.firstname.visibility = 'visible';
            errorMessage.firstname.text = '*İsim alanı boş bırakılamaz';
            status = false;
        } else {
            errorMessage.firstname.visibility = 'collapse';
            errorMessage.firstname.text = '';
        }

        if(!this.user.lastname.trim()) {
            errorMessage.lastname.visibility = 'visible';
            errorMessage.lastname.text = '*Soyisim alanı boş bırakılamaz';
            status = false;
        } else {
            errorMessage.lastname.visibility = 'collapse';
            errorMessage.lastname.text = '';
        }

        if(this.user.gender == '0') {
            errorMessage.gender.visibility = 'visible';
            errorMessage.gender.text = '*Cinsiyet seçiniz';
            status = false;
        } else {
            errorMessage.gender.visibility = 'collapse';
            errorMessage.gender.text = '';
        }

        if(!this.user.birthdate) {
            errorMessage.birthDate.visibility = 'visible';
            errorMessage.birthDate.text = '*Doğum Tarihi Seçiniz';
            status = false;
        } else {
            errorMessage.birthDate.visibility = 'collapse';
            errorMessage.birthDate.text = '';
        }

        if(this.user.country_id == '0') {
            errorMessage.countryID.visibility = 'visible';
            errorMessage.countryID.text = '*Ülke seçiniz';
            status = false;
        } else {
            errorMessage.countryID.visibility = 'collapse';
            errorMessage.countryID.text = '';
        }

        if(this.user.city_id == 0 || !this.user.city_id || this.user.city_id == null) {
            errorMessage.cityID.visibility = 'visible';
            errorMessage.cityID.text = '*Şehir seçiniz';
            status = false;
        } else {
            errorMessage.cityID.visibility = 'collapse';
            errorMessage.cityID.text = '';
        }

        if(this.user.district_id == 0 || !this.user.district_id || this.user.district_id == null) {
            errorMessage.districtID.visibility = 'visible';
            errorMessage.districtID.text = '*İlçe seçiniz';
            status = false;
        } else {
            errorMessage.districtID.visibility = 'collapse';
            errorMessage.districtID.text = '';
        }

        if(!this.user.telephone.trim()) {
            errorMessage.telephone.visibility = 'visible';
            errorMessage.telephone.text = '*Telefon alanı boş bırakılamaz';
            status = false;
        } else {
            errorMessage.telephone.visibility = 'collapse';
            errorMessage.telephone.text = '';
        }

        if(!this.user.organisation.trim()) {
            errorMessage.organisation.visibility = 'visible';
            errorMessage.organisation.text = '*Çalıştığın Kurum alanı boş bırakılamaz';
            status = false;
        } else {
            errorMessage.organisation.visibility = 'collapse';
            errorMessage.organisation.text = '';
        }

        if(!this.user.title.trim()) {
            errorMessage.title.visibility = 'visible';
            errorMessage.title.text = '*Meslek alanı boş bırakılamaz';
            status = false;
        } else {
            errorMessage.title.visibility = 'collapse';
            errorMessage.title.text = '';
        }

        return status;
    }
    
    loadUser() {
        this.loading = true;
        this.userService.getUser()
            .subscribe((result) => {
                this.user = result['items'][0];
                this.loadDistricts(this.user.city_id, this.user.district_id);
                this.loadCities(this.user.city_id);
                this.selectedGender = this.genders.getIndex(this.user.gender)
                this.selectedCountry = this.countries.getIndex(`${this.user.country_id}`)
                this.loading = false;
            }, (error) => {
                console.log(error);
                this.loading = false;
            });
    }

    loadCities(city_id: number) {
        this.userService.getCities()
            .subscribe((result) => {
                let keys = Object.keys(result);
                let cities = [];

                cities.push({value: '0', display: 'Seçiniz...'});
                for (let loop = 0; loop < keys.length; loop++) {
                    cities.push({
                        value: `${result[loop].city_code}`,
                        display: result[loop].city_name
                    });
                }

                this.cities = new ValueList<string>(cities);
                this.selectedCity = this.cities.getIndex(`${city_id}`)
            }, (error) => {
                console.log(error);
            });
    }

    loadDistricts(city_id: number, district_id: number) {
        this.userService.getDistricts(city_id)
            .subscribe((result) => {
                let keys = Object.keys(result);
                let districts = [];

                districts.push({value: '0', display: 'Seçiniz...'});
                for (let loop = 0; loop < keys.length; loop++) {
                    districts.push({
                        value: `${result[loop].id}`,
                        display: result[loop].district_name
                    });
                }

                this.districts = new ValueList<string>(districts);
                this.selectedDistrict = this.districts.getIndex(`${district_id}`)
            }, (error) => {
                console.log(error);
            });
    }

    genderChange(args: SelectedIndexChangedEventData) {
        this.user.gender = this.genders.getValue(args.newIndex);
    }

    countryChange(args: SelectedIndexChangedEventData) {
        this.user.country_id = this.countries.getValue(args.newIndex);
    }

    cityChange(args: SelectedIndexChangedEventData) {
        let city_id = Number(this.cities.getValue(args.newIndex));
        this.user.city_id = city_id;

        this.loadDistricts(city_id, 0)
    }

    districtChange(args: SelectedIndexChangedEventData) {
        this.user.district_id = Number(this.districts.getValue(args.newIndex))
    }

    informRookiesChange(modelRef) {
        this.user.inform_rookies = modelRef.checked;
    }

    informActivityChange(modelRef) {
        this.user.inform_activity = modelRef.checked;
    }

    informCorporateActivityChange(modelRef) {
        this.user.inform_corporate_activity = modelRef.checked;
    }

    beVolunteerChange(modelRef) {
        this.user.be_volunteer = modelRef.checked;
    }

    birthDateChange(args) {
        if(args.value != 'Invalid Date') {
            this.user.birthdate = this.getFormattedDate(args.value)
        }
    }

    getFormattedDate(date: Date) {
        return date.getFullYear() + '-' + ('0' + (date.getMonth()+1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2)
    }

}
