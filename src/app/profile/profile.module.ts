import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { NativeScriptFormsModule } from "nativescript-angular/forms"

import { ProfileMainComponent } from './profile-main/profile-main.component';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';

@NgModule({
    declarations: [ProfileMainComponent, ProfileEditComponent],
    imports: [
        NativeScriptCommonModule,
        NativeScriptRouterModule,
        NativeScriptFormsModule,
        NativeScriptRouterModule.forChild([
            { path: '', redirectTo: 'profile-main', pathMatch: 'full' },
            { path: 'profile-main', component: ProfileMainComponent },
            { path: 'profile-edit', component: ProfileEditComponent },
        ]),
        TNSFontIconModule.forRoot({
			'la': require('~/assets/line-awesome.min.css'),
        }),
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class ProfileModule { }
