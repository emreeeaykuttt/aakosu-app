import { Component, OnInit } from '@angular/core';
import { confirm } from "tns-core-modules/ui/dialogs";
import { RouterExtensions } from "nativescript-angular/router";
import * as localStorage from 'nativescript-localstorage';

import { UserService } from '../../services/user.service';
import { NotificationService } from '~/app/services/notification.service';
import { User } from '../../models/user.model';

@Component({
    selector: 'ProfileMain',
    templateUrl: './profile-main.component.html',
    styleUrls: ['./profile-main.component.css']
})
export class ProfileMainComponent implements OnInit {

    loading = false;
    deviceToken = localStorage.getItem('device_token');
    title = 'Başlık';
    description = 'Açıklama';
    user: User;

    constructor(private userService: UserService, private notificationService: NotificationService, private routerExtensions: RouterExtensions) {
        this.user = new User();
    }

    ngOnInit(): void {
        this.loadUser();
    }
    
    loadUser() {
        this.loading = true;
        this.userService.getUser()
            .subscribe((result) => {
                this.user = result['items'][0];
                this.user.organisation = this.user.organisation ? this.user.organisation + ' - ' : '';
                this.loading = false;
            }, (error) => {
                console.log(error);
                this.loading = false;
            });
    }

    logout() {
        let options = {
            title: 'Çıkış',
            message: 'Emin misiniz',
            okButtonText: 'Çıkış Yap',
            neutralButtonText: 'Vazgeç'
        };
        
        confirm(options).then((result: boolean) => {
            if(result) {
                this.userService.logout()
                this.routerExtensions.navigate(['/home'], {clearHistory: true});
            }
        });
    }

    testNotification() {
        this.notificationService.sendNotification(this.deviceToken, this.title, this.description)
            .subscribe((result) => {
                alert('success: ' + result['success'])
            });
    }

}
