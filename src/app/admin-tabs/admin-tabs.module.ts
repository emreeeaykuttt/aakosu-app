import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptRouterModule, NSEmptyOutletComponent } from 'nativescript-angular/router';
import { NativeScriptCommonModule } from 'nativescript-angular/common';

import { AdminTabsComponent } from './admin-tabs.component';

@NgModule({
    declarations: [AdminTabsComponent],
    imports: [
        NativeScriptCommonModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forChild([
            {
                path: 'admin-default', component: AdminTabsComponent, children: [
                    {
                        path: 'admin-race-list',
                        outlet: 'adminRaceTab',
                        component: NSEmptyOutletComponent,
                        loadChildren: () => import('~/app/admin-race/admin-race.module').then(m => m.AdminRaceModule),
                    },
                    {
                        path: 'admin-trail-list',
                        outlet: 'adminTrailTab',
                        component: NSEmptyOutletComponent,
                        loadChildren: () => import('~/app/admin-trail/admin-trail.module').then(m => m.AdminTrailModule),
                    },
                    {
                        path: 'admin-notification-users',
                        outlet: 'adminNotificationTab',
                        component: NSEmptyOutletComponent,
                        loadChildren: () => import('~/app/admin-notification/admin-notification.module').then(m => m.AdminNotificationModule),
                    },
                ]
            }
        ])
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class AdminTabsModule { }
