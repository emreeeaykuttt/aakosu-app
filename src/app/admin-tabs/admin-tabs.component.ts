import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { ActivatedRoute } from '@angular/router';
import { Page } from "tns-core-modules/ui/page";

@Component({
    moduleId: module.id,
    selector: 'AdminTabsPage',
    templateUrl: './admin-tabs.component.html',
    styleUrls: ['./admin-tabs.component.css']
})
export class AdminTabsComponent implements OnInit {

    constructor(
        private routerExtension: RouterExtensions,
        private activeRoute: ActivatedRoute,
        private hideActionbar:  Page) {
    }

    ngOnInit() {
        this.routerExtension.navigate([{ outlets: { adminRaceTab: ['admin-race-list'], adminTrailTab: ['admin-trail-list'], adminNotificationTab: ['admin-notification-users'] } }], { relativeTo: this.activeRoute });
        this.hideActionbar.actionBarHidden = true;
    }

    goBack() {
        // this.routerExtension.navigate(['/tabs/default', {outlets: { profileTab: ['profile-main'] }}], {clearHistory: true, });
        this.routerExtension.navigate(['/tabs/default'], {clearHistory: true, animated: true, transition: { name: 'flip', duration: 1000, curve: 'linear' }});
    }

}
