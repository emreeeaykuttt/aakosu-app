import { NgModule, NO_ERRORS_SCHEMA, ErrorHandler, NgModuleFactoryLoader } from '@angular/core';
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';
import { TNSCheckBoxModule } from '@nstudio/nativescript-checkbox/angular';
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import { NativeScriptHttpClientModule } from 'nativescript-angular/http-client';
import { NSModuleFactoryLoader } from 'nativescript-angular/router';
import { DropDownModule } from "nativescript-drop-down/angular";
import { NativeScriptDateTimePickerModule } from "nativescript-datetimepicker/angular";
import { enable as traceEnable, addCategories } from 'tns-core-modules/trace';
import { AppComponent } from './app.component';

import { AppRoutingModule, COMPONENTS } from './app-routing.module';
import { AuthGuard } from './auth-guard.service';
import { UserService } from './services/user.service';
import { RaceService } from './services/race.service';
import { RaceUserService } from './services/race-user.service';
import { NotificationService } from './services/notification.service';

traceEnable();

export class MyErrorHandler implements ErrorHandler {
    handleError(error) {
        console.log('### ErrorHandler Error: ' + error.toString());
        console.log('### ErrorHandler Stack: ' + error.stack);
    }
}

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        TNSCheckBoxModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        DropDownModule,
        NativeScriptDateTimePickerModule,
    ],
    declarations: [
        AppComponent,
        ...COMPONENTS,
    ],
    providers: [
        AuthGuard,
        UserService,
        RaceService,
        RaceUserService,
        NotificationService,
        { provide: ErrorHandler, useClass: MyErrorHandler },
        { provide: NgModuleFactoryLoader, useClass: NSModuleFactoryLoader }
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule { }
