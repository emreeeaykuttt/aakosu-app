import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { NativeScriptUIListViewModule } from 'nativescript-ui-listview/angular';
import { NativeScriptFormsModule } from "nativescript-angular/forms"

import { AdminNotificationUsersComponent } from './admin-notification-users/admin-notification-users.component';
import { AdminNotificationSendComponent } from './admin-notification-send/admin-notification-send.component';

@NgModule({
    declarations: [AdminNotificationUsersComponent, AdminNotificationSendComponent],
    imports: [
        NativeScriptCommonModule,
        NativeScriptUIListViewModule,
        NativeScriptFormsModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forChild([
            { path: '', redirectTo: 'admin-notification-users', pathMatch: 'full' },
            { path: 'admin-notification-users', component: AdminNotificationUsersComponent },
            { path: 'admin-notification-send', component: AdminNotificationSendComponent },
        ]),
        TNSFontIconModule.forRoot({
			'la': require('~/assets/line-awesome.min.css'),
        }),
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class AdminNotificationModule { }
