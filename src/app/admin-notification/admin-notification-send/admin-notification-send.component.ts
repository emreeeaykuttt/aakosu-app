import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { Subscription } from 'rxjs';
import * as dialogs from 'tns-core-modules/ui/dialogs';

import { NotificationService } from '~/app/services/notification.service';
import { Notification } from '~/app/models/notification.model';


@Component({
    selector: 'AdminNotificationSend',
    moduleId: module.id,
    templateUrl: './admin-notification-send.component.html',
    styleUrls: ['./admin-notification-send.component.css']
})
export class AdminNotificationSendComponent implements OnInit {

    loading = false;
    userIds = '';
    userDeviceTokens = '';
    notification: Notification;
    subscription: Subscription;

    constructor(
        private notificationService: NotificationService,
        private route: ActivatedRoute,
        private routerExtension: RouterExtensions,
    ) {
        this.route.queryParams.subscribe(params => {
            this.userIds = params['userIds'];
            this.userDeviceTokens = params['userDeviceTokens'];
        });
     }

    ngOnInit(): void {
        this.notification = new Notification();
    }

    onNavBtnTap(){
        this.routerExtension.navigate(['admin-notification-users'], { relativeTo: this.route.parent });
    }

    sendNotification() {
        this.notificationService.saveNotification(this.notification, this.userIds, this.userDeviceTokens)
            .subscribe((result) => {
                console.log(result)
                if (result['status']) {
                    dialogs.alert({
                        title: 'Başarılı',
                        message: result['message'],
                        okButtonText: 'Tamam',
                        cancelable: false 
                    }).then(() => {
                        this.routerExtension.navigate(['admin-notification-users'], { relativeTo: this.route.parent });
                    });
                } else {
                    dialogs.alert({
                        title: 'Uyarı',
                        message: result['message'],
                        okButtonText: 'Tamam',
                        cancelable: false 
                    });
                }
            },(error) => {
                console.log(error);
            });
    }

}
