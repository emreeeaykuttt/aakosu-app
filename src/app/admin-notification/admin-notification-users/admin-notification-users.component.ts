import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationExtras } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { ObservableArray } from 'tns-core-modules/data/observable-array';
import { RadListViewComponent } from 'nativescript-ui-listview/angular';
import { ListViewEventData } from 'nativescript-ui-listview';
import { EventData } from 'tns-core-modules/data/observable';
import { TextView } from 'tns-core-modules/ui/text-view';
import * as dialogs from 'tns-core-modules/ui/dialogs';

import { UserService } from '~/app/services/user.service';
import { User } from '~/app/models/user.model';

@Component({
    selector: 'AdminNotificationUsers',
    moduleId: module.id,
    templateUrl: './admin-notification-users.component.html',
    styleUrls: ['./admin-notification-users.component.css']
})
export class AdminNotificationUsersComponent implements OnInit {

    loading = false;
    userIdArray = [];
    userDeviceTokenArray = [];
    descriptionText = 'Yukarıdaki alana bildirim göndermek istediğiniz kişinin adını yazınız.';
    warningText = 'En az 3 karakter girmelisiniz.';
    user: User;
    _userItems: ObservableArray<User> = new ObservableArray([]); 
    @ViewChild('userList', { static: false }) userList: RadListViewComponent;

    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private routerExtension: RouterExtensions,
    ) { }

    public get userItems(): ObservableArray<User> {
        return this._userItems;
    }

    ngOnInit(): void {
        
    }

    loadUserList(runLoading: boolean = true, searchText: string = '') {
        this.loading = runLoading;
        this.userService.getUsers(searchText)
            .subscribe((result) => {
                this._userItems = new ObservableArray([]);
                if (result != '') {
                    this.warningText = '';
                    this._userItems.push(result);
                } else {
                    this.descriptionText = '';
                    this.warningText = 'Aradığınız kullanıcı bulunamadı';
                }
                this.loading = false;
            }, (error) => {
                console.log(error);
            });
    }

    onPullToRefreshInitiated(args: ListViewEventData) {
        const that = new WeakRef(this);
        
        setTimeout(function () {
            that.get()._userItems = new ObservableArray([])
            that.get().loadUserList(false);
            const listView = args.object;
            listView.notifyPullToRefreshFinished();
        }, 1000);
    }

    userSearch(args: EventData) {
        const searchText = args.object as TextView;
        if (searchText.text.length > 2) {
            this.loadUserList(true, searchText.text);
        } else {
            this.descriptionText = 'Yukarıdaki alana bildirim göndermek istediğiniz kişinin adını yazınız.';
            this.warningText = 'En az 3 karakter girmelisiniz.';
        }
    }

    onItemSelected(args: ListViewEventData) {
        let itemSelected = this._userItems.getItem(args.index);
        this.userIdArray.push(itemSelected.id);
        this.userDeviceTokenArray.push(itemSelected.device_token);
    }

    onItemDeselected(args: ListViewEventData) {
        let itemSelected = this._userItems.getItem(args.index);
        let userIdVal = this.userIdArray.indexOf(itemSelected.id);
        let userDeviceTokenVal = this.userDeviceTokenArray.indexOf(itemSelected.device_token);

        this.userIdArray.splice(userIdVal, 1);
        this.userDeviceTokenArray.splice(userDeviceTokenVal, 1);
    }

    usersGet() {
        if (this.userIdArray.length > 0) {
            this.routerExtension.navigate(['admin-notification-send'], { relativeTo: this.route.parent , queryParams: {userIds: JSON.stringify(this.userIdArray), userDeviceTokens: JSON.stringify(this.userDeviceTokenArray)} });
        } else {
            dialogs.alert({
                title: 'Uyarı',
                message: 'En az bir kişiye seçmek zorundasınız!',
                okButtonText: "Tamam",
                cancelable: false
            });
        }
    }
}
