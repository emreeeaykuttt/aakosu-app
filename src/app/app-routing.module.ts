import { NgModule } from '@angular/core';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { Routes } from '@angular/router';

import { AuthGuard } from './auth-guard.service';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './home/login.component';
import { RegisterComponent } from './home/register.component';

export const COMPONENTS = [HomeComponent, LoginComponent, RegisterComponent];

const routes: Routes = [
    { path: '', redirectTo: '/tabs/default', pathMatch: 'full' },
    // { path: '', redirectTo: '/admin-tabs/admin-default', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'tabs', loadChildren: () => import('~/app/tabs/tabs.module').then(m => m.TabsModule), canActivate: [AuthGuard]},
    { path: 'admin-tabs', loadChildren: () => import('~/app/admin-tabs/admin-tabs.module').then(m => m.AdminTabsModule), canActivate: [AuthGuard]},
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
